/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#include "Logger.h"

// qt header
#include <QtCore/QDir>
#include <QtCore/QMutex>
#include <QtCore/QTextStream>
#include <QtCore/QDateTime>

// system header
#include <iostream>
#include <fstream>

namespace XPIWIT
{

// initialize the singleton instance
Logger* Logger::m_Instance = 0;

// the constructor
Logger::Logger( QObject* parent ) : QObject( parent ), coutStream( NULL ), m_GlobalLogFile()
{
    m_LogFileExists = false;
    m_GlobalLogExists = false;
    m_GlobalLogHeaderExists = false;
}


// the destructor
Logger::~Logger()
{
    std::cout.rdbuf( coutbuf );       //reset to standard output again
    std::cerr.rdbuf( cerrbuf );       //reset to standard output again

    if( m_GlobalLogFile.isOpen() )
        m_GlobalLogFile.close();
}


// return the singleton instance
Logger *Logger::GetInstance( QObject* parent )
{
    static QMutex mutex;
    if (!m_Instance)
    {
        mutex.lock();

        if (!m_Instance)
            m_Instance = new Logger( parent );

        mutex.unlock();
    }

    return m_Instance;
}


// delete the instance
void Logger::DeleteInstance()
{
    static QMutex mutex;
    mutex.lock();
    if(m_Instance)
	{
        delete m_Instance;
        m_Instance = 0;
    }
    mutex.unlock();
}


// initialize the log file
void Logger::InitLogFile( bool fileLogging, QString rootDirectory, QString fileName )
{
    // check if directory exist
    if( rootDirectory.isEmpty() || fileName.isEmpty() )
        fileLogging = false;

    if( !fileLogging )
	{
        WriteLine( "Error while init log: file can not be created. File name: " + rootDirectory + fileName );
        return;
    }

    coutbuf = std::cout.rdbuf(); //save old buffer
    cerrbuf = std::cerr.rdbuf(); //save old buffer

    // create internal cout stream
    coutStream.rdbuf( coutbuf );

    // add subfolder
    if( m_UseSubfolder )
	{
        QString subFolder("result");
        QDir outputDir( rootDirectory );
        if( !( outputDir.exists( subFolder ) || outputDir.mkdir( subFolder ) ) )	// if folder doesn't exists and can not be created
            throw QString( "Error while creating subfolder. Path: " + rootDirectory + subFolder );
        rootDirectory += subFolder + "/";
    }

    // create file name
    QString stdLogPath = rootDirectory;
    stdLogPath += fileName;
    stdLogPath += "_" + QDateTime::currentDateTime().toString( "yyyy-MM-dd_hh-mm-ss" ) + QString("_PipelineLog") + QString(".txt");		// "dd_MM_yy_hh_mm"

    // redirect stdout to the logfile
    std::ofstream *coutFile = new std::ofstream( stdLogPath.toStdString().c_str(), std::ios_base::app );
    coutFile->open( stdLogPath.toStdString().c_str() );
    filebuf = coutFile->rdbuf();
    std::cout.rdbuf( filebuf );

    m_LogFileExists = true;
}


// set the log file to a new directory
void Logger::SetNewLogFile( QString rootDirectory, QString fileName )
{
    // check if directory exist
    if( rootDirectory.isEmpty() || fileName.isEmpty() )
	{
        WriteLine( "Error while init log: file can not be created. File name: " + rootDirectory + fileName );
        return;
    }

    // create file name
    QString stdLogPath = rootDirectory;
    if( m_UseSubfolder )
        stdLogPath += "result/";
    stdLogPath += fileName;
    stdLogPath += "_" + QDateTime::currentDateTime().toString( "yyyy-MM-dd_hh-mm-ss" ) + QString("_PipelineLog") + QString(".txt");	// "dd_MM_yy_hh_mm"

    // redirect stdout to the logfile
    std::ofstream *coutFile = new std::ofstream( stdLogPath.toStdString().c_str(), std::ios_base::app );
    coutFile->open( stdLogPath.toStdString().c_str() );
    filebuf = coutFile->rdbuf();
    std::cout.rdbuf( filebuf );

    m_LogFileExists = true;
}


// release the log file
void Logger::ReleaseLogFile()
{
    if( !m_LogFileExists )
        return;

    std::cout.rdbuf( coutbuf );       //reset to standard output again
    std::cerr.rdbuf( cerrbuf );       //reset to standard output again

    m_LogFileExists = false;
}


// write a line to cout
void Logger::Write( QString line )
{
    std::cout << line.toStdString();
}


// write a line to cout including a line break
void Logger::WriteLine( QString line )
{
    std::cout << line.toStdString() << std::endl;
}


// write an std::exception object
void Logger::WriteException( std::exception err )
{
	std::cout << err.what() << std::endl;
}


// write a spacer
void Logger::WriteSpacer()
{
    if( m_LogFileExists )
	{
        std::cout << "-----------------------------------------------------------------------------------------------" << std::endl;
    }
	else
	{
        std::cout << "------------------------------------------------------------" << std::endl;   // shorter for console
    }
}


// write to the console
void Logger::WriteToConsole( QString line )
{
    if( m_LogFileExists )
	{
        coutStream << line.toStdString() << std::endl;
    }
	else
	{
        std::cout << line.toStdString() << std::endl;
    }
}


// write to console and cout
void Logger::WriteToAll( QString line )
{
    if( m_LogFileExists )
	{
        coutStream << line.toStdString() << std::endl;
    }
    std::cout << line.toStdString() << std::endl;
}


// initialize the global log file
void Logger::InitGlobalLog( QString rootDirectory, QString fileName, QString separator )
{
    m_CSVSeparator = separator;

    QString globalLogPath = rootDirectory;
    if( m_UseSubfolder )
        globalLogPath += "result/";
    globalLogPath += fileName;
    globalLogPath += QDateTime::currentDateTime().toString( "yyyy-MM-dd_hh-mm-ss" ) + QString("_GlobalLog") + QString(".csv");

    m_GlobalLogFile.setFileName( globalLogPath );
    m_GlobalLogExists = true;
}


// write a line to the global log file
void Logger::WriteGlobalLine( QStringList line, bool hasImageOutput, QStringList metaFiles )
{
    // check if log exist
    if( !m_GlobalLogExists )
	{
        WriteLine( "Error while writting to global log: global log not initialized" );
        return;
    }

    // try to open it
    if( !m_GlobalLogFile.open( QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text ) )
	{
        WriteLine( "Error while writting to global log: file can not be opened (created)" );
        return;
    }

    // write header if not already exist
    if( !m_GlobalLogHeaderExists )
        WriteGlobalLogHeader( hasImageOutput, metaFiles );

    // iterate over string list and add separator to create a single string
    QString tempLine = "";
    for( int i = 0; i < line.length(); i++ )
	{
        tempLine += line.at(i);
        if( (i+1) < line.length() ) // add separator if not at end
            tempLine += m_CSVSeparator;
    }

    // write data and close the file
    m_GlobalLogFile.write( QString(tempLine + "\n").toUtf8() );
    m_GlobalLogFile.close();
}


// write header of the global log file
void Logger::WriteGlobalLogHeader( bool hasImageOutput, QStringList metaFiles )
{
    //Write Global log header
    QStringList headerLine;
    if( hasImageOutput )
        headerLine << "id" << "processing time sec" << "input image" << "output image";
    else
        headerLine << "id" << "processing time sec" << "input image";

    // add the csv file names
    foreach( QString i_str, metaFiles )
        headerLine.append("csv " + i_str);

    // iterate over string list and add separator to create a single string
    QString tempLine = "";
    for( int i = 0; i < headerLine.length(); i++ )
	{
        tempLine += headerLine.at(i);
        if( (i+1) < headerLine.length() ) // add separator if not at end
            tempLine += m_CSVSeparator;
    }

    // write data to file
    m_GlobalLogFile.write( QString(tempLine + "\n").toUtf8() );
    m_GlobalLogHeaderExists = true;
}

}
