/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifndef CMDARGUMENTS_H
#define CMDARGUMENTS_H

// namespace header
#include "CMDInOutArgument.h"
#include "CMDPipelineArguments.h"

// qt header
#include <QtCore/QList>
#include <QtCore/QString>
#include <QtCore/QStringList>


namespace XPIWIT
{

/**
 * @class CMDArguments
 * @brief The CMDArguments class represents a cmd/piped input as it was delivered.
 */
class CMDArguments
{
public:
    CMDArguments();

    bool mWriteFilterList;
    QString mFilterListPath;

    QString mXmlPath;

    bool mLockFileEnabled;
    bool mFileLoggingEnabled;
    bool mUseSubFolder;

    int mSeed;

    QStringList mSubFolderFormat;
    QStringList mOutputFileFormat;

    // input
    QList< CMDInOutArgument *> mInputArguments;

    // output
    CMDInOutArgument *mOutputArgument;
	bool mSkipProcessingIfOutputExists;

    // meta data
    bool mUseMetaDataHeader;
    QString mMetaDataSeparator;
    QString mMetaDataDelimitor;


    int mNumberOfImages;
    bool CheckInputs();

    CMDPipelineArguments *GetPipelineArguments( int );
    int GetNumberOfImages();

    void WriteToLog();
    QString BoolToString( bool );

	void CreateInputFolder();

private:

};

} //namespace XPIWIT

#endif // CMDARGUMENTS_H

