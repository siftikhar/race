/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// file header
#include "CMDParser.h"

// namespace header
#include "../Utilities/Logger.h"
#include "CMDInOutArgument.h"

// qt header
#include <QtCore/QCoreApplication>
#include <QtCore/QDir>

// system header
#include <iostream>
using namespace std;


namespace XPIWIT
{

// default constructor
CMDParser::CMDParser()
{
	// init bools
	mWriteFilterList = false;
	mProcessPipeline = false;

	// init key
    mEnableKey << "1" << "true" << "on";
    mDisableKey << "0" << "false" << "off";
    mSubFolderKeys << "filterid" << "filtername";
    mOutputFormatKeys << "prefix" << "imagename" << "filterid" << "filtername";
}


// the destructor
CMDParser::~CMDParser()
{
}


// function to initialize the cmd arguments
CMDArguments *CMDParser::InitArguments()
{
    // two general modes -> command line parameter || piped parameter
    QStringList arguments = QCoreApplication::arguments();

    int startIndex = 0;

    // checking if first argument is programm name
    QRegExp mRxIsExe(  "\\.exe$" );
    if( !mRxIsExe.exactMatch( arguments.at(0) ))
        startIndex = 1;

	// either wait and store piped arguments or process cmd arguments
    if( arguments.length() <= startIndex )
	{
        std::cout << "Processing piped arguments" << std::endl;
        arguments = PipedArguments();
    }
	else
	{
        std::cout << "Processing cmd arguments" << std::endl;
    }

    // parse the given arguments
	try
	{
		// maybe a good idea for timeout
		//int timeout = 500; // msecs
		//singleShot( timeout, this, SLOT(mySlot());
		ProcessArguments( arguments );
	}
	catch( QString ex )
	{
        Logger::GetInstance()->WriteLine( ex + "\n");
    }

    return mCmdArguments;
}


// process piped arguments
QStringList CMDParser::PipedArguments()
{
    QStringList arguments;
    QString currentParameter;

	// TODO: implement timer to start after waiting a while?

    // reading input from cin
    while ( !cin.fail() )
	{
        // read input
        string tmp;
        cin >> tmp;

        // convert to QString
        QString qtmp = QString::fromStdString( tmp );

        // end condition
        if ( qtmp.compare( "--end" ) == 0 )
            break;

        // add the read command
        if ( qtmp.left( 2 ).compare( "--" ) == 0 || qtmp.left( 1 ).compare( "%" ) == 0 )   // new command?
		{
            // add all previously read parameter
            if( currentParameter.compare("") != 0 )
                arguments.append( currentParameter );
            currentParameter = "";

            // add new command
            arguments.append( qtmp );
            continue;
        }

        // add the input to the parameter list
        currentParameter += qtmp + " ";
    }

    // add last parameter
    if( currentParameter.compare("") != 0 )
        arguments.append( currentParameter );

    return arguments;
}

void CMDParser::ProcessArguments( QStringList arguments )
{
	Logger *log = Logger::GetInstance();
	mProcessPipeline = true;

    mCmdArguments = new CMDArguments();

    //-- filter list --//
    //-----------------//
    QStringList valueList = ProcessParameter( arguments, "--filterlist");
	if( valueList.isEmpty() || valueList.at( 0 ).isEmpty() )
	{
        mCmdArguments->mWriteFilterList = false;
        mCmdArguments->mFilterListPath = "";
		mWriteFilterList = false;
    }
    else
	{
        mCmdArguments->mWriteFilterList = true;
        mCmdArguments->mFilterListPath = valueList.at( 0 );
		mWriteFilterList = true;
    }

    //--- xml ---//
    //-----------//
    valueList = ProcessParameter( arguments, "--xml");
    // check if the list is set
    if( valueList.isEmpty() || valueList.length() > 1 )
	{
		// processing can be aborted at this point
		mProcessPipeline = false;
        throw( QString("Warning: CMDProcessing::ProcessArguments - No xml specified!") );
    }
    // set the path of the xml
    mCmdArguments->mXmlPath = valueList.at(0);
    //-----------//


    //-- logging --//
    //--------------//
    valueList = ProcessParameter( arguments, "--logging");
    mCmdArguments->mFileLoggingEnabled = ProcessFlagParameter( valueList );
    //--------------//

    //-- lockfile --//
    //--------------//
    valueList = ProcessParameter( arguments, "--lockfile");
    mCmdArguments->mLockFileEnabled = ProcessFlagParameter( valueList, false );
    //--------------//

    //-- input --//
    //-----------//
    ProcessIO( arguments, "--input" );
    //-----------//

    //-- output --//
    //------------//
    valueList = ProcessParameter( arguments, "--output");

    if( valueList.isEmpty() || valueList.length() > 2 )
	{
		mProcessPipeline = false;
        throw( QString("Error: CMDProcessing::ProcessArguments - No output specified!") );
	}

    CMDInOutArgument *outputArg = new CMDInOutArgument();
    outputArg->InitOutput( valueList );
    mCmdArguments->mOutputArgument = outputArg;

	valueList = ProcessParameter( arguments, "--skipProcessingIfOutputExists");
	mCmdArguments->mSkipProcessingIfOutputExists = ProcessFlagParameter( valueList, false );
    //------------//

    //-- seed --//
    //----------//
    valueList = ProcessParameter( arguments, "--seed");
    if( valueList.isEmpty() || valueList.length() > 1 )    // default
	{
        mCmdArguments->mSeed = 0;
    }
	else
	{
        mCmdArguments->mSeed = valueList.at(0).toInt();
    }
    //----------//

    //-- subfolder --//
    //---------------//
    valueList = ProcessParameter( arguments, "--subfolder");
    if( valueList.isEmpty() )
	{
        mCmdArguments->mUseSubFolder = true;
		mCmdArguments->mSubFolderFormat << "filterid" << "filtername";
	}
	else
	{

		if( mDisableKey.contains( valueList.at(0) ) )
		{
			mCmdArguments->mUseSubFolder = false;
		}
		else
		{
			mCmdArguments->mUseSubFolder = true;
			mCmdArguments->mSubFolderFormat << "filterid" << "filtername";

			// check inputs
			for( int i = 0; i < valueList.length(); i++  )
			{
				if( !mSubFolderKeys.contains( valueList.at(i) ) )
					log->WriteLine( "Warning: unknown subfolder key will be skipped: " + valueList.at(i) );
			}
			mCmdArguments->mSubFolderFormat = valueList;
		}
    }

    //-- output format --//
    //-------------------//
    valueList = ProcessParameter( arguments, "--outputformat");
    if( valueList.isEmpty() )
	{
        mCmdArguments->mOutputFileFormat << "prefix" << "imagename" << "filterid" << "filtername";
    }
	else
	{
        // check inputs
        for( int i = 0; i < valueList.length(); i++  )
		{
            if( !mOutputFormatKeys.contains( valueList.at(i) ) )
                log->WriteLine( "Warning: unknown output file name key: " + valueList.at(i) );
        }
        mCmdArguments->mOutputFileFormat = valueList;
    }

    //-- Meta Data --//
    valueList = ProcessParameter( arguments, "--metaDataHeader");
    if( valueList.isEmpty() )
	{
        mCmdArguments->mUseMetaDataHeader = true;
    }
	else
	{
        mCmdArguments->mUseMetaDataHeader = true;
        if( mDisableKey.contains( valueList.at(0) ) )
            mCmdArguments->mUseMetaDataHeader = false;
    }

    valueList = ProcessParameter( arguments, "--metaDataSeparator");
    if( valueList.isEmpty() )
	{
        mCmdArguments->mMetaDataSeparator = ";";
    }
	else
	{
        mCmdArguments->mMetaDataSeparator = valueList.at(0);
    }

    valueList = ProcessParameter( arguments, "--metaDataDelimiter");
    if( valueList.isEmpty() )
	{
        mCmdArguments->mMetaDataDelimitor = ".";
    }
	else
	{
        mCmdArguments->mMetaDataDelimitor = valueList.at(0);
    }

    if( !mCmdArguments->CheckInputs() )
        throw QString( "Error: in inputs validation" );
}


// function to process a single parameter
QStringList CMDParser::ProcessParameter(QStringList arguments, QString argument)
{
    // returns the parameter list for a given argument
    if( arguments.contains( argument) )
	{
        // get the index of the command
        int index = arguments.indexOf( argument );
        // parameter list is at the following index
        return ProcessValues( arguments, ++index );
    }

    return QStringList();
}


// function to process a parameter value
QStringList CMDParser::ProcessValues( QStringList arguments, int index )
{
    if( arguments.at( index ).startsWith("-"))
		Logger::GetInstance()->WriteLine( "Error: CMDProcessing::ProcessValues for " + arguments.at( index ) );

    QStringList list;
    // split by comma and delete whitespace
    foreach( QString i_str, arguments.at( index ).split(",") )
        list.append( i_str.replace( " ", "" ) );

    return list;
}


// function to process t he input and output arguments
void CMDParser::ProcessIO( QStringList arguments, QString argument )
{
    int nextIndex = 0;
    nextIndex = arguments.indexOf( argument, nextIndex ) + 1;

    while( nextIndex > 0 )
	{
        //-- get input --//
        QStringList valueList = ProcessValues( arguments, nextIndex );
        // prepare for next iteration
        nextIndex = arguments.indexOf( argument, nextIndex ) + 1;

        if( valueList.isEmpty() )
		{
			mProcessPipeline = false;
			Logger::GetInstance()->WriteLine( "Error while processing --input argument" );
            Logger::GetInstance()->WriteLine( "Error: CMDProcessing::ProcessIO at index: " + QString::number(nextIndex) );
        }

        CMDInOutArgument *inOutArgument = new CMDInOutArgument();
        inOutArgument->InitInput( valueList );
        mCmdArguments->mInputArguments.append( inOutArgument );
    }
}


// function to process boolean parameters
bool CMDParser::ProcessFlagParameter( QStringList arguments, bool defaultFlag )
{
    if( arguments.isEmpty() )
        return defaultFlag;

    QString argument = arguments.at( 0 );

    if( mEnableKey.contains( argument, Qt::CaseInsensitive ))
        return true;

    if( mDisableKey.contains( argument, Qt::CaseInsensitive ))
        return false;

    // no matches
    return defaultFlag;
}

} // XPIWIT namespace
