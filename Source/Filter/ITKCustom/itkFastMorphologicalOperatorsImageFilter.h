/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifndef __XPIWITCORE_FASTMORPHOLOGICALOPERATORSIMAGEFILTER_H
#define __XPIWITCORE_FASTMORPHOLOGICALOPERATORSIMAGEFILTER_H

// include required headers
#include "../Base/Management/ITKDefinitions.h"

#include "itkExtractImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkMorphologicalWatershedImageFilter.h"
#include <string>
#include <math.h>
#include <float.h>

using namespace XPIWIT;

namespace itk {

template <class TImageType, class TOutputImage, class TStructuringElement>
class ITK_EXPORT FastMorphologicalOperatorsImageFilter : public ImageToImageFilter<TImageType, TOutputImage>
{
	public:
		/** Extract dimension from input and output image. */
		itkStaticConstMacro(ImageDimension, unsigned int, TImageType::ImageDimension);

		enum FilterMode
        {
            FILTERMODE_EROSION = 0,
            FILTERMODE_DILATION = 1,
            FILTERMODE_CLOSING = 2,
            FILTERMODE_OPENING = 3,
        };

		typedef FastMorphologicalOperatorsImageFilter Self;
		typedef ImageToImageFilter<TImageType,TOutputImage> Superclass;
		typedef SmartPointer<Self> Pointer;
		typedef SmartPointer<const Self> ConstPointer;

		itkNewMacro(Self);

		itkGetMacro( Type, unsigned int);
        itkSetMacro( Type, unsigned int);
		itkGetMacro( Kernel, TStructuringElement);
        itkSetMacro( Kernel, TStructuringElement);
		itkGetMacro( IgnoreBorderRegions, bool);
        itkSetMacro( IgnoreBorderRegions, bool);

		void SetMaskImage(const TImageType* image)        { m_MaskImage = image; }

		typedef TImageType InputImageType;
		typedef typename TImageType::PixelType PixelType;

		typedef typename TImageType::RegionType InputImageRegionType;
		typedef typename TOutputImage::RegionType OutputImageRegionType;

	protected:
		FastMorphologicalOperatorsImageFilter();
		virtual ~FastMorphologicalOperatorsImageFilter();
		
		/**
		 * Functions for data generation.
		 */
		void ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId);
		//unsigned int SplitRequestedRegion(unsigned int i, unsigned int num, OutputImageRegionType& splitRegion);

		/** 
		 * functions for prepareing and post-processing the threaded generation.
		 */
		void BeforeThreadedGenerateData();
		void AfterThreadedGenerateData();

		// member variables
		unsigned int m_Type;
		TStructuringElement m_Kernel;
		const TImageType* m_MaskImage;
		bool m_IgnoreBorderRegions;
};

} /* namespace itk */

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkFastMorphologicalOperatorsImageFilter.hxx"
#endif

#endif
