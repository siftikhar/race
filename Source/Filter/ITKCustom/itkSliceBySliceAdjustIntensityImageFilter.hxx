/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// include required headers
#include "itkSliceBySliceAdjustIntensityImageFilter.h"
#include "itkImageRegionIterator.h"
#include "itkImageRegionConstIterator.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include <iostream>
#include <fstream>
#include <string>
#include <limits>
#include <QString>
#include <QStringList>
#include <QDateTime>
#include "../../Core/Utilities/Logger.h"

// namespace itk
namespace itk
{

// the default constructor
template <class TImageType, class TOutputImage>
SliceBySliceAdjustIntensityImageFilter<TImageType, TOutputImage>::SliceBySliceAdjustIntensityImageFilter()
{
	m_ScaleToMinMax = false;
	m_Quantile = -1.0;
	m_MinSlice = 0;
	m_MaxSlice = 100000;
	m_DebugOutput = false;
}


// the destructor
template <class TImageType, class TOutputImage>
SliceBySliceAdjustIntensityImageFilter<TImageType, TOutputImage>::~SliceBySliceAdjustIntensityImageFilter()
{
}


// before threaded generate data
template <class TImageType, class TOutputImage>
void SliceBySliceAdjustIntensityImageFilter<TImageType, TOutputImage>::BeforeThreadedGenerateData()
{
    // Allocate output
    typename TOutputImage::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();
}


// the thread generate data
template <class TImageType, class TOutputImage>
void SliceBySliceAdjustIntensityImageFilter<TImageType, TOutputImage>::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId)
{
    // get input and output pointers
    typename TOutputImage::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();
	
	// initialize the regions to process
	typename TImageType::RegionType currentRegion;
	typename TImageType::IndexType currentIndex;
	typename TImageType::SizeType currentSize;

	for (int j=0; j<TImageType::ImageDimension; ++j)
	{
		currentIndex[j] = outputRegionForThread.GetIndex(j);
		currentSize[j] = outputRegionForThread.GetSize(j);
	}
	currentSize[2] = 1;
	currentRegion.SetIndex( currentIndex );
	currentRegion.SetSize( currentSize );
	
	// create a temporary image used to extract slices
	typename TImageType::Pointer tmpImage = TImageType::New();
	tmpImage->SetRegions(currentRegion);
	tmpImage->SetSpacing( input->GetSpacing() );
	tmpImage->Allocate();
	tmpImage->FillBuffer(0);

	// iterate over all slices and perform watershed separately for each slice
	for (int i=0; i<outputRegionForThread.GetSize(2); ++i)
	{
		// set the current index
		currentIndex[2] = outputRegionForThread.GetIndex(2)+i;
		currentRegion.SetIndex( currentIndex );
		currentRegion.SetSize( currentSize );

		// only process the valid range of slices
		if (currentIndex[2] < m_MinSlice || currentIndex[2] > m_MaxSlice)
			continue;

		// extract the slice to perform the watershed on
		ImageRegionConstIterator<TImageType> inputIterator( input, currentRegion );
		ImageRegionIterator<TImageType> sliceIterator( tmpImage, tmpImage->GetLargestPossibleRegion() );
		inputIterator.GoToBegin();
		sliceIterator.GoToBegin();

		// initialize the histogram
		unsigned int histogram[65535] = {};
		double intensitySum = 0.0;
		typename TImageType::PixelType minimum = itk::NumericTraits< typename TImageType::PixelType >::max();
		typename TImageType::PixelType maximum = 0;
		
		while(inputIterator.IsAtEnd() == false)
		{
			// copy the input value
			sliceIterator.Set( inputIterator.Value() );

			// update minimum and maximum values
			if (inputIterator.Value() < minimum) 
				minimum = inputIterator.Value();

			// update minimum and maximum values
			if (maximum < inputIterator.Value()) 
				maximum = inputIterator.Value();

			// update the sum of intensities
			if (typeid( typename TImageType::PixelType ) == typeid( float ) || typeid( typename TImageType::PixelType ) == typeid( double ))
				intensitySum += inputIterator.Value()*65535;
			else
				intensitySum += inputIterator.Value();

			// increment the iterators
			++inputIterator;
			++sliceIterator;
		}

		// check if minimum and maximum are valid
		if ( (maximum-minimum) < itk::NumericTraits< float >::epsilon())
			continue;

		inputIterator.GoToBegin();
		while(inputIterator.IsAtEnd() == false)
		{
			// increment the respective histogram bin
			if (typeid( typename TImageType::PixelType ) == typeid( float ) || typeid( typename TImageType::PixelType ) == typeid( double ))
				++histogram[int(65534*((float)inputIterator.Value()))];
			else
				++histogram[(int)inputIterator.Value()];

			++inputIterator;
		}

		// calculate the quantiles for scaling
		unsigned int numPixels = currentSize[0]*currentSize[1];
		float lowerQuantile = m_Quantile;
		float upperQuantile = 1.0 - m_Quantile;
		double quantileIntensity = 0.0;
		float lowerQuantileIntensity = 0.0;
		float upperQuantileIntensity = 0.0;
		for (int j=0; j<65535; ++j)
		{
			quantileIntensity += ((double)histogram[j])/((double)numPixels);

			if (quantileIntensity <= lowerQuantile)
				lowerQuantileIntensity = ((float)j)/65535.0;

			if (quantileIntensity <= upperQuantile)
				upperQuantileIntensity = ((float)j)/65535.0;
		}
		
		if (m_DebugOutput == true)
			std::cout << "- LowerQuantile: " << lowerQuantileIntensity << ", UpperQuantile: " << upperQuantileIntensity << ", Min: " << minimum << ", Max: " << maximum << std::endl;

		// setup the intensity filter
		typedef itk::IntensityWindowingImageFilter<TImageType, TImageType> IntensityFilterType;
		typename IntensityFilterType::Pointer rescaleIntensity = IntensityFilterType::New();
		rescaleIntensity->SetInput( tmpImage );

		// set output range
		if( typeid( typename TOutputImage::PixelType ) == typeid( float ) ||
			typeid( typename TOutputImage::PixelType ) == typeid( double ) ) {
			// floating point types
			rescaleIntensity->SetOutputMinimum( itk::NumericTraits< typename TOutputImage::PixelType >::ZeroValue() );
			rescaleIntensity->SetOutputMaximum( itk::NumericTraits< typename TOutputImage::PixelType >::OneValue() );
		}else{
			// integral types
			rescaleIntensity->SetOutputMinimum( itk::NumericTraits< typename TOutputImage::PixelType >::min() );
			rescaleIntensity->SetOutputMaximum( itk::NumericTraits< typename TOutputImage::PixelType >::max() );
		}				

		// set the windowing parameters
		if (m_ScaleToMinMax == true)
		{
			rescaleIntensity->SetWindowMinimum( minimum );
			rescaleIntensity->SetWindowMaximum( maximum );
		}
		else if (m_Quantile > 0)
		{
			rescaleIntensity->SetWindowMinimum( std::max<float>(lowerQuantileIntensity, 0.0) );
			rescaleIntensity->SetWindowMaximum( std::min<float>(1.0, upperQuantileIntensity) );
		}
		else
		{
			rescaleIntensity->SetWindowMinimum( rescaleIntensity->GetOutputMinimum() );
			rescaleIntensity->SetWindowMaximum( rescaleIntensity->GetOutputMaximum() );
		}

		// update the filter
		itkTryCatch( rescaleIntensity->Update(), "Exception Caught: Updating slice based intensity adjustment filter on a slice." );

		// initialize the seed region iterator
		ImageRegionIterator<TOutputImage> rescaleIterator( rescaleIntensity->GetOutput(), rescaleIntensity->GetOutput()->GetLargestPossibleRegion() );
		ImageRegionIterator<TOutputImage> outputIterator( output, currentRegion );

		rescaleIterator.GoToBegin();
		outputIterator.GoToBegin();

		// iterate trough the seed region and set voxels for the initial LSF
		float currentValue = 0.0;
		while (outputIterator.IsAtEnd() == false)
		{
			currentValue = rescaleIterator.Value();
			if (currentValue >= 0.0 && currentValue <= 1.0)
				outputIterator.Set( currentValue );
			else
				outputIterator.Set( 0.0 );

			++outputIterator;
			++rescaleIterator;
		}
	}
}


// after threaded generate data
template <class TImageType, class TOutputImage>
void SliceBySliceAdjustIntensityImageFilter<TImageType, TOutputImage>::AfterThreadedGenerateData()
{
    // maybe peform post processing here?
}

} // end namespace itk
