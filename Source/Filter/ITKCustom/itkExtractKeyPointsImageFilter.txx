/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifndef __XPIWIT_EXTRACTKEYPOINTSIMAGEFILTER_HXX
#define __XPIWIT_EXTRACTKEYPOINTSIMAGEFILTER_HXX

// include required headers
#include "itkExtractKeyPointsImageFilter.h"
#include "itkConstNeighborhoodIterator.h"
#include "itkNeighborhoodInnerProduct.h"
#include "itkImageRegionIterator.h"
#include "itkNeighborhoodAlgorithm.h"
#include "itkOffset.h"
#include "itkProgressReporter.h"
#include <fstream>
#include <iostream>
#include <QtCore/QFile>
#include <QtCore/QTextStream>
#include "../../Core/Utilities/Logger.h"

using namespace XPIWIT;

// namespace itk
namespace itk
{

// the default constructor
template< class TInputImage, class TOutputImage, class TMaximumScaleImage >
ExtractKeyPointsImageFilter< TInputImage, TOutputImage, TMaximumScaleImage >::ExtractKeyPointsImageFilter()
{
    mAllowMaximumPlateaus = false;
    mMaximumScaleImage = NULL;
    mNeighborhoodRadius = 1;
    mMinimumSeedCombinations = 1;
    mThreshold = -1;
    mQuantile = -1;
    mStdDevMultiplicator = 2;
    mNeighborhood3D = true;
    mFileName = "";
    mFuseSeedPoints = false;
    mRemove2DSeedsTouching3DSeeds = false;
    mMetaDataFilter = NULL;
}


// the destructor
template< class TInputImage, class TOutputImage, class TMaximumScaleImage >
ExtractKeyPointsImageFilter< TInputImage, TOutputImage, TMaximumScaleImage >::~ExtractKeyPointsImageFilter()
{
    itk::ThreadIdType numThreads = this->GetNumberOfThreads();
    for (itk::ThreadIdType i=0; i<numThreads; ++i)
    {
        mKeyPoints[i].clear();
    }

    delete[] mKeyPoints;
}

template< class TInputImage, class TOutputImage, class TMaximumScaleImage >
float ExtractKeyPointsImageFilter< TInputImage, TOutputImage, TMaximumScaleImage >::CalculateQuantile(const float quantile)
{
    // get the input
    typename TInputImage::ConstPointer input  = this->GetInput();

    ImageRegionConstIterator<TInputImage> inputIterator( input, input->GetLargestPossibleRegion() );
    inputIterator.GoToBegin();
    unsigned int overallNumberOfPixels = 1;
    for (int i=0; i<TInputImage::ImageDimension; ++i)
        overallNumberOfPixels *= input->GetLargestPossibleRegion().GetSize()[i];

    unsigned int numPixels = 0;
    unsigned int histogramSize = 65535;
    unsigned int* histogram = new unsigned int[histogramSize];
    for (int i=0; i<histogramSize; ++i)
        histogram[i] = 0;

    while (inputIterator.IsAtEnd() == false)
    {
        histogram[std::max<int>(0,std::min<int>(65534, (int)(0.5+inputIterator.Get() * histogramSize)))]++;
        numPixels++;
        ++inputIterator;
    }

    float lowerQuantile = 0.0;
    float upperQuantile = 1.0;
    float numLower = 0.0;
    float numUpper = 0.0;
    float binWidth = 1.0 / (float)histogramSize;
    for (int i=0; i<histogramSize; ++i)
    {
        if (lowerQuantile <= 0.0 && (numLower/(float)overallNumberOfPixels) >= quantile)
        {
            lowerQuantile = (float)i / (float)histogramSize;
            break;
        }

        numLower += histogram[i];

        //std::cout << "Lower: " << 100.0*(numLower/(float)overallNumberOfPixels) << ", NumVoxels: " << numLower << std::endl;
    }

    for (int i=0; i<histogramSize; ++i)
    {
        if ((numUpper/(float)overallNumberOfPixels) >= quantile)
        {
            upperQuantile = (float)(histogramSize-i-1) / (float)histogramSize;
            break;
        }

        numUpper += histogram[histogramSize-i-1];

        //std::cout << "Upper: " << 100.0*(numUpper/(float)overallNumberOfPixels) << ", NumVoxels: " << numUpper << std::endl;
    }

    delete[] histogram;

    // log quantile information
    Logger::GetInstance()->WriteLine( "- Lower Quantile: " + QString::number(lowerQuantile) + ", Upper Quantile: " + QString::number(upperQuantile) );

    // return the lower quantile
    return lowerQuantile;
}

// before threaded generate data
template< class TInputImage, class TOutputImage, class TMaximumScaleImage >
void ExtractKeyPointsImageFilter< TInputImage, TOutputImage, TMaximumScaleImage >::BeforeThreadedGenerateData()
{
    // get the input
    typename TInputImage::ConstPointer input  = this->GetInput();

    // initialize the seed array for each thread
    itk::ThreadIdType numThreads = this->GetNumberOfThreads();
    mKeyPoints = new std::vector<typename NeighborhoodIteratorType::IndexType>[numThreads];
    mSeedPoints2D = new std::vector< SeedPointType>[numThreads];
    mSeedPoints3D = new std::vector< SeedPointType>[numThreads];

    // extract the mean of the input image which is used as seed threshold if -1 is specified
    if (mThreshold < 0 && mQuantile < 0)
    {
        // calculate histogram and scale to the selected quantiles
        ImageRegionConstIterator<InputImageType> inputIterator( input, input->GetLargestPossibleRegion() );
        inputIterator.GoToBegin();
        unsigned int numPixels = 1;
        for (int i=0; i < TInputImage::ImageDimension; ++i)
            numPixels *= input->GetLargestPossibleRegion().GetSize()[i];
        double sumOfIntensities = 0.0;
        double sumOfMeanSubtractedIntensities = 0.0;

        // fill the histogram
        while (inputIterator.IsAtEnd() == false)
        {
            sumOfIntensities += inputIterator.Get();
            ++inputIterator;
        }

        // calculate the mean value
        double mean = sumOfIntensities / (float)numPixels;

        // reset the iterator and calculate the sum of mean normalized intensity values
        inputIterator.GoToBegin();
        while (inputIterator.IsAtEnd() == false)
        {
            sumOfMeanSubtractedIntensities += (inputIterator.Get()-mean)*(inputIterator.Get()-mean);
            ++inputIterator;
        }


        double sigma = sqrtf( sumOfMeanSubtractedIntensities / ((float)numPixels-1) );
        mThreshold = mean + mStdDevMultiplicator*sigma;

        // show new calculated threshold
        Logger::GetInstance()->WriteLine( "+ Using mean based threshold, which is set to: " + QString::number(mThreshold) );
    }
    else if (mQuantile > 0)
    {
        float quantile = CalculateQuantile( mQuantile );
        mThreshold = quantile;
    }

    // fill the output buffer with zeros
    typename OutputImageType::Pointer output = this->GetOutput();
    output->FillBuffer(0);
}


// the thread generate data
template< class TInputImage, class TOutputImage, class TMaximumScaleImage >
void ExtractKeyPointsImageFilter< TInputImage, TOutputImage, TMaximumScaleImage >::ThreadedGenerateData(const OutputImageRegionType & outputRegionForThread, ThreadIdType threadId)
{
    // define input and output image pointers
    typename InputImageType::ConstPointer input  = this->GetInput();
    typename OutputImageType::Pointer output = this->GetOutput();

    // define the output region to be without the border
    typename OutputImageType::RegionType regionWithoutBorders;
    typename OutputImageRegionType::IndexType regionWithoutBordersIndex;
    typename OutputImageRegionType::SizeType regionWithoutBordersSize;
    for (int i=0; i<TInputImage::ImageDimension; ++i)
    {
        regionWithoutBordersIndex[i] = outputRegionForThread.GetIndex(i)+mNeighborhoodRadius;
        regionWithoutBordersSize[i] = outputRegionForThread.GetSize(i)-2*mNeighborhoodRadius;
    }
    /*
    regionWithoutBordersIndex[1] = outputRegionForThread.GetIndex(1)+mNeighborhoodRadius;
    regionWithoutBordersIndex[2] = outputRegionForThread.GetIndex(2)+mNeighborhoodRadius;

    regionWithoutBordersSize[1] = outputRegionForThread.GetSize(1)-2*mNeighborhoodRadius;
    regionWithoutBordersSize[2] = outputRegionForThread.GetSize(2)-2*mNeighborhoodRadius;
    */

    // set the region
    regionWithoutBorders.SetIndex(regionWithoutBordersIndex);
    regionWithoutBorders.SetSize(regionWithoutBordersSize);

    // perform 3d seed point detection followed by 2d detection.
    // pixels in the direct neighborhood of a 3d seed will be skipped by 2d detection.
    for (int i=0; i<2; ++i)
    {
        // specify the neighborhood by the provided radius
        typename NeighborhoodIteratorType::RadiusType radius;
        radius.Fill( mNeighborhoodRadius );

        // check if extraction should be performed at all
        if (mNeighborhood3D != 2 && i != mNeighborhood3D)
            continue;

        // remove 3rd dimension of the neighborhood iterator for 2d detection.
        if (i == 0 && input->GetImageDimension() == 3)
            radius[2] = 0;

        // specify input and output iterators
        ConstNeighborhoodIterator< InputImageType > inputIterator( radius, input, outputRegionForThread );
        ImageRegionIterator< OutputImageType > outputIterator( output, outputRegionForThread );
        ImageRegionConstIterator< MaximumScaleImageType > maximumScaleIterator( mMaximumScaleImage, outputRegionForThread );

        // get the number of neighbours and the center pixel index
        unsigned int numNeighbors = inputIterator.Size();
        unsigned int centerIndex = (numNeighbors-1)/2;

        // initialize current value and maximum flag
        InputRealType currentValue = 0.0;
        bool maximum = true;

        // reset the iterators
        inputIterator.GoToBegin();
        outputIterator.GoToBegin();
        maximumScaleIterator.GoToBegin();

        // temporary variables for bound checking of neighborhood pixels
        //typename ConstNeighborhoodIterator< InputImageType >::OffsetType internalIndex;
        //typename ConstNeighborhoodIterator< InputImageType >::OffsetType offset;

        // neighborhood loop variable
        unsigned int j;

        // loop trough the region
        while ( !inputIterator.IsAtEnd() )
        {
            // get current value
            currentValue = static_cast< InputRealType >( inputIterator.GetCenterPixel() );
            maximum = true;

            // if value is below threshold skip further calculations
            if (currentValue <= mThreshold) // || outputIterator.Value() > 0)
            {
                ++inputIterator;
                ++outputIterator;
                ++maximumScaleIterator;
                continue;
            }

            int currentScale = maximumScaleIterator.Value();

            // check if current pixel is greater than the neighbors
            //numPixels = inputIterator.GetSize()[0]*inputIterator.GetSize()[1]*inputIterator.GetSize()[2];
            for (j=0; j<numNeighbors; ++j)
            {
                if (j != centerIndex &&
                        (inputIterator.GetPixel(j) > currentValue ||
                         (inputIterator.GetPixel(j) >= currentValue && mAllowMaximumPlateaus == false)))
                {
                    maximum = false;
                    break;
                }
            }

            // store the keypoint if it is a maximum
            if (maximum == true)
            {
                /*for (j=0; j<numNeighbors; ++j)
                {
                    if (inputIterator.IndexInBounds(j, internalIndex, offset) == true)
                    {
                        output->SetPixel( inputIterator.GetIndex( j ), (1.0/9.0)*currentScale * (0.5/((float)i+1)) );
                    }
                }*/

                mKeyPoints[threadId].push_back( inputIterator.GetIndex() );

                // add seed point to the corresponding array
                if (i == 0)
                    mSeedPoints2D[threadId].push_back( SeedPointType(inputIterator.GetIndex(), maximumScaleIterator.Value(), 0, currentValue, false) );
                else
                    mSeedPoints3D[threadId].push_back( SeedPointType(inputIterator.GetIndex(), maximumScaleIterator.Value(), 0, currentValue, true) );

                outputIterator.Set( currentValue );
            }

            // increase the iterators
            ++inputIterator;
            ++outputIterator;
            ++maximumScaleIterator;
        }
    }
}


// after threaded generate data
template< class TInputImage, class TOutputImage, class TMaximumScaleImage >
void ExtractKeyPointsImageFilter< TInputImage, TOutputImage, TMaximumScaleImage >::AfterThreadedGenerateData()
{
    // get the output
    typename InputImageType::ConstPointer input  = this->GetInput();
    typename OutputImageType::Pointer output = this->GetOutput();

    // get the spacing of the image
    typename InputImageType::SpacingType spacing = input->GetSpacing();

    // create file for writing the keypoints
//    std::ofstream keyPointsFile;
//    std::ofstream globalKeyPointsFile;
//    keyPointsFile.open( mFileName.toLatin1().data() );

    // merge keypoints of the threads
    unsigned int num3DSeedPoints = 0;
    unsigned int numSeedPointsPerThread = 0;
    unsigned int currentSeedPointID = 0;
    unsigned int x = 0;
    unsigned int y = 0;
    unsigned int z = 0;

    // perform the fusion of detected 2d and 3d seeds. 3d seeds are prefered but are more unlikely to be found
    std::vector<SeedPointType> originalSeedPoints;
    std::vector<int> originalSeedPointCombinations;
    std::vector<int> mergeIndex;

    // add the 3d seed points of all threads to the seed point file
    itk::ThreadIdType numThreads = this->GetNumberOfThreads();
    for (itk::ThreadIdType i=0; i<numThreads; ++i)
    {
        numSeedPointsPerThread = mSeedPoints3D[i].size();
        num3DSeedPoints += numSeedPointsPerThread;

        for (int j=0; j<numSeedPointsPerThread; ++j)
        {
            // increase keypoint counter
            ++currentSeedPointID;

            // convert seed to physical space and apply offset if specified
            NeighborhoodIteratorIndexType index = mSeedPoints3D[i][j].GetIndex();

            for (int k=0; k<TInputImage::ImageDimension; ++k)
                index[k] = index[k]*spacing[k] + mKeyPointOffset;

            // add the seed point to the seed vector
            originalSeedPoints.push_back( SeedPointType(index, mSeedPoints3D[i][j].GetScale(), currentSeedPointID, mSeedPoints3D[i][j].GetIntensity(), true) );
            originalSeedPointCombinations.push_back(3);
            mergeIndex.push_back(-1);
        }
    }

    // display the number of nuclei found in 3d scale space
    Logger::GetInstance()->WriteLine( "+ Found " + QString::number(num3DSeedPoints) + " seed points in 3d neighborhood." );

    // variable for the current seed point
    float dist[TInputImage::ImageDimension];
    for(int i=0; i<TInputImage::ImageDimension; ++i)
        dist[i] = 0.0;
    float distance = 0.0;
    float minDistance = 1e19;
    float seedPointMinDistance = 0.0;
    int minIndex = -1;
    int num2DSeedPoints = 0;
    int currentSeedScale = 0;
    bool deleteSeed = false;
    NeighborhoodIteratorIndexType currentSeedIndex;

    // iterate trough the 2d seeds per thread and add them as well
    for (itk::ThreadIdType i=0; i<numThreads; ++i)
    {
        // get the number of thread seed points
        numSeedPointsPerThread = mSeedPoints2D[i].size();
        num2DSeedPoints += numSeedPointsPerThread;

        // loop through all seed points
        for (int j=0; j<numSeedPointsPerThread;++j)
        {
            // get the current seed point
            deleteSeed = false;
            currentSeedScale = mSeedPoints2D[i][j].GetScale();
            currentSeedIndex = mSeedPoints2D[i][j].GetIndex();
            for(int k=0; k<TInputImage::ImageDimension; ++k)
                currentSeedIndex[k] = currentSeedIndex[k]*spacing[k] + mKeyPointOffset;

            // check if seed point is in the scale radius sphere of a 3d seed
            if (mRemove2DSeedsTouching3DSeeds == true)
            {
                for (int k=0; k<num3DSeedPoints; ++k)
                {
                    // update the minimum distance according to the maximum of the seed point radii. If the distance of two centroids
                    // is smaller than the maximum radius, one of the centroids clearly lies within the bounding sphere of the other seed.
                    seedPointMinDistance = 1.41 * (originalSeedPoints[k].GetScale() + currentSeedScale);

                    // calculate the x,y,z distances with spacing correction
                    for (int l=0; l<TInputImage::ImageDimension; ++l)
                        dist[l] = abs(currentSeedIndex[l]-originalSeedPoints[k].GetIndex()[l]);

                    if (TInputImage::ImageDimension == 3)
					{
                        // don't fuse the seed with itself or very far seeds
                        if (dist[0] > seedPointMinDistance || dist[1] > seedPointMinDistance || dist[2] > seedPointMinDistance)
                            continue;

                        // calculate the distance
                        distance = sqrtf( float(powf(dist[0],2.0) + powf(dist[1],2.0) + powf(dist[2],2.0)));
                        // TODO: Check Scaling!!!!!!!
                    }
					else if (TInputImage::ImageDimension == 2)
					{
						// don't fuse the seed with itself or very far seeds
                        if (dist[0] > seedPointMinDistance || dist[1] > seedPointMinDistance)
                            continue;

						distance = sqrtf( float(powf(dist[0],2.0) + powf(dist[1],2.0)) );
					}


                    // break if mindistance is already smaller than threshold
                    if (distance < seedPointMinDistance)
                    {
                        deleteSeed = true;
                        break;
                    }
                }
            }

            // add the seed if delete is not set
            if (deleteSeed == false)
            {
                // increase seed point id
                ++currentSeedPointID;

                // add the seed point to the seed vector
                originalSeedPoints.push_back( SeedPointType(currentSeedIndex, mSeedPoints2D[i][j].GetScale(), currentSeedPointID, mSeedPoints2D[i][j].GetIntensity(), false) );
                originalSeedPointCombinations.push_back(1);
                mergeIndex.push_back(-1);
            }
        }
    }

    // display the number of nuclei found in 2d scale space
    Logger::GetInstance()->WriteLine( "+ Found " + QString::number(num2DSeedPoints) + " seed points in 3d neighborhood." );

    if (mRemove2DSeedsTouching3DSeeds == true)
    {
       Logger::GetInstance()->WriteLine( "+ Found (" + QString::number(currentSeedPointID - num3DSeedPoints) +
                                             " / " + QString::number(num2DSeedPoints) +
                                             ") seed points in 2d neighborhood that were not touching a 3d seed point within the seed radius." );
       Logger::GetInstance()->WriteLine( "+ Discarded (" + QString::number(num2DSeedPoints - (currentSeedPointID - num3DSeedPoints)) +
                                             " / " + QString::number(num2DSeedPoints) + ") seed points due to being in the radius bounding sphere of a 3d seed point." );
    }

    // fuse nearby seed points according to a minimum distance
    int numFusedSeeds = 0;
    if (mFuseSeedPoints == true && mergeIndex.size() > 0)
    {
        int currentIndex = 0;
        bool finished = false;

        // loop through the seeds and combine according to minimum distance
        while (finished == false)
        {
            // reinit min distance and min index
            minDistance = 1e19;
            minIndex = -1;

            // skip seed if it was already combined previously
            if (mergeIndex[currentIndex] >= 0)
            {
                ++currentIndex;

                // check if maximal index is reached
                if (currentIndex == originalSeedPoints.size())
                    finished = true;

                continue;
            }

            // find the minimal seed point
            const int numOriginalSeedPoints = originalSeedPoints.size();
            for (int i=0; i<numOriginalSeedPoints;++i)
            {
                // skip seeds that were already merged
                //if (originalSeedPointCombinations[i] < 0 || i == currentIndex)
                if (i == currentIndex || mergeIndex[i] == currentIndex)
                    continue;

                // update the minimum distance according to the maximum of the seed point radii. If the distance of two centroids
                // is smaller than the maximum radius, one of the centroids clearly lies within the bounding sphere of the other seed.
                seedPointMinDistance = 1.41 * std::max<int>(originalSeedPoints[currentIndex].GetScale(), originalSeedPoints[i].GetScale());
                //seedPointMinDistance = 1.41 * (originalSeedPoints[currentIndex].GetScale() + originalSeedPoints[i].GetScale());

                // calculate the x,y,z distances with spacing correction
                for (int j=0; j<TInputImage::ImageDimension; ++j)
                    dist[j] = abs(originalSeedPoints[currentIndex].GetIndex()[j]-originalSeedPoints[i].GetIndex()[j]);

                if (TInputImage::ImageDimension == 3)
				{
                    // don't fuse the seed with itself or very far seeds
                    if (dist[0] > seedPointMinDistance || dist[1] > seedPointMinDistance || dist[2] > seedPointMinDistance)
                        continue;

                    // calculate the distance
                    distance = sqrtf( float(powf(dist[0],2.0) + powf(dist[1],2.0) + powf(dist[2],2.0)));
                }
				else if (TInputImage::ImageDimension == 2)
				{
                    // don't fuse the seed with itself or very far seeds
                    if (dist[0] > seedPointMinDistance || dist[1] > seedPointMinDistance)
                        continue;

                    // calculate the distance
                    distance = sqrtf( float(powf(dist[0],2.0) + powf(dist[1],2.0)) );
                }

                // update the minimum distance
                if (distance < minDistance)
                {
                    minDistance = distance;
                    minIndex = i;
                }

                // break if mindistance is already smaller than threshold
                //			if (minDistance < seedPointMinDistance)
                //					break;
            }

            // if seeds are closer than the minimum distance they are merged
            // iterative mean calculation is used, i.e. it is stored how many elements were used to form the mean so far.
            // mu1 = 1/2*(value1+value2); mu1 = 1/3*(2*mu1 + value3); mu2 = 1/4*(3*mu2 + value4), ...
            // otherwise each new seed point has more influence than the other ones.
            if (minDistance < seedPointMinDistance)
            {
                if (originalSeedPoints[currentIndex].GetIntensity() <= originalSeedPoints[minIndex].GetIntensity())
                {
                    if (mergeIndex[minIndex] >= 0)
                    {
                        mergeIndex[currentIndex] = mergeIndex[minIndex];
                        originalSeedPointCombinations[mergeIndex[minIndex]]++;
                        originalSeedPointCombinations[minIndex] = -1;
                    }
                    else
                    {
                        mergeIndex[currentIndex] = minIndex;
                        originalSeedPointCombinations[minIndex]++;
                    }

                    // update indices if local maxima assignemtns change
                    for (int i=0; i<originalSeedPoints.size(); ++i)
                    {
                        if (mergeIndex[i] == currentIndex)
                            mergeIndex[i] = mergeIndex[currentIndex];
                    }

                    originalSeedPointCombinations[currentIndex] = -1;
                }
                else
                {
                    mergeIndex[minIndex] = currentIndex;
                    originalSeedPointCombinations[currentIndex]++;
                    originalSeedPointCombinations[minIndex] = -1;
                }

                ++numFusedSeeds;
            }
            else
            {
                // increase the object index
                ++currentIndex;

                // check if maximal index is reached
                if (currentIndex == originalSeedPoints.size())
                    finished = true;
            }

            /*
            // find the minimal seed point
            for (int i=currentIndex; i<originalSeedPoints.size();++i)
            {
                // skip seeds that were already merged
                if (originalSeedPointCombinations[i] < 0)
                    continue;

                // update the minimum distance according to the maximum of the seed point radii. If the distance of two centroids
                // is smaller than the maximum radius, one of the centroids clearly lies within the bounding sphere of the other seed.
                seedPointMinDistance = 1.41 * std::max<int>(originalSeedPoints[currentIndex].GetScale(), originalSeedPoints[i].GetScale());
                //seedPointMinDistance = 1.41 * (originalSeedPoints[currentIndex].GetScale() + originalSeedPoints[i].GetScale());

                // calculate the x,y,z distances with spacing correction
                xDist = originalSeedPoints[currentIndex].GetIndex()[0]-originalSeedPoints[i].GetIndex()[0];
                yDist = originalSeedPoints[currentIndex].GetIndex()[1]-originalSeedPoints[i].GetIndex()[1];
                zDist = originalSeedPoints[currentIndex].GetIndex()[2]-originalSeedPoints[i].GetIndex()[2];

                // don't fuse the seed with itself or very far seeds
                if (i == currentIndex || xDist > seedPointMinDistance || yDist > seedPointMinDistance || zDist > seedPointMinDistance)
                    continue;

                // calculate the distance
                distance = sqrtf( float(powf(xDist,2.0) + powf(yDist,2.0) + powf(zDist,2.0)));

                // update the minimum distance
                if (distance < minDistance)
                {
                    minDistance = distance;
                    minIndex = i;
                }

                // break if mindistance is already smaller than threshold
                if (minDistance < seedPointMinDistance)
                    break;
            }

            // if seeds are closer than the minimum distance they are merged
            // iterative mean calculation is used, i.e. it is stored how many elements were used to form the mean so far.
            // mu1 = 1/2*(value1+value2); mu1 = 1/3*(2*mu1 + value3); mu2 = 1/4*(3*mu2 + value4), ...
            // otherwise each new seed point has more influence than the other ones.
            if (minDistance < seedPointMinDistance)
            {
                float scalingFactor = 1.0/(originalSeedPointCombinations[currentIndex]+1);
                typename InputImageType::IndexType combinedIndex;
                combinedIndex[0] = int(0.5 + scalingFactor * (originalSeedPointCombinations[currentIndex]*originalSeedPoints[currentIndex].GetIndex()[0]+originalSeedPoints[minIndex].GetIndex()[0]));
                combinedIndex[1] = int(0.5 + scalingFactor * (originalSeedPointCombinations[currentIndex]*originalSeedPoints[currentIndex].GetIndex()[1]+originalSeedPoints[minIndex].GetIndex()[1]));
                combinedIndex[2] = int(0.5 + scalingFactor * (originalSeedPointCombinations[currentIndex]*originalSeedPoints[currentIndex].GetIndex()[2]+originalSeedPoints[minIndex].GetIndex()[2]));

                // combine the seed points
                SeedPointType combinedSeed = SeedPointType( combinedIndex,
                                                            int(0.5 + scalingFactor * (originalSeedPointCombinations[currentIndex]*originalSeedPoints[currentIndex].GetScale()+originalSeedPoints[minIndex].GetScale())),
                                                            originalSeedPoints[currentIndex].GetID(),
                                                            scalingFactor * (originalSeedPointCombinations[currentIndex]*originalSeedPoints[currentIndex].GetIntensity()+originalSeedPoints[minIndex].GetIntensity()) );

                // update the combined seed
                originalSeedPoints[currentIndex] = combinedSeed;
                //originalSeedPoints.erase(originalSeedPoints.begin()+minIndex);
                originalSeedPointCombinations[currentIndex]++;
                originalSeedPointCombinations[minIndex] = -1;
                ++numFusedSeeds;
            }
            else
            {
                // increase the object index
                ++currentIndex;

                // check if maximal index is reached
                if (currentIndex == originalSeedPoints.size())
                    finished = true;
            }*/
        }
    }

    if( mMetaDataFilter != NULL ){
        QStringList metaDescription;                    QStringList metaType;
        metaDescription << "id";						metaType << "int";
        metaDescription << "scale";                     metaType << "int";
        metaDescription << "xpos";                      metaType << "int";
        metaDescription << "ypos";                      metaType << "int";
        metaDescription << "zpos";                      metaType << "int";
        metaDescription << "intensity";                 metaType << "float";
        metaDescription << "seedPoint3D";               metaType << "int";
        metaDescription << "seedPointCombinations";     metaType << "int";

        mMetaDataFilter->mTitle = metaDescription;
        mMetaDataFilter->mType = metaType;
    }

    mMetaDataFilter->mPostfix = "KeyPoints";

    // write the remaining seeds to disk
    int numValidSeeds = 0;
    for (int j=0; j<originalSeedPoints.size(); ++j)
    {
        if (originalSeedPointCombinations[j] >= mMinimumSeedCombinations)
        {
            typename OutputImageType::IndexType index;
            for(int i = 0; i < TInputImage::ImageDimension; i++)
                index[i] = (originalSeedPoints[j].GetIndex()[i]-mKeyPointOffset)/spacing[i];

            output->SetPixel(index, 1.0);

            int newScale = originalSeedPoints[j].GetScale();
//            keyPointsFile << j << "," << newScale << "," << int(0.5+originalSeedPoints[j].GetIndex()[0]) << "," << int(0.5+originalSeedPoints[j].GetIndex()[1]) << "," << int(0.5+originalSeedPoints[j].GetIndex()[2]) << "," << originalSeedPoints[j].GetIntensity() << "," << originalSeedPoints[j].GetSeedPoint3D() << "," << originalSeedPointCombinations[j] << std::endl;

            if( mMetaDataFilter != NULL ){
                // new meta handling
                QList<float> dataSet;
                dataSet << j;
                dataSet << newScale;
                dataSet << int(0.5+originalSeedPoints[j].GetIndex()[0]);
                dataSet << int(0.5+originalSeedPoints[j].GetIndex()[1]);
                dataSet << int(0.5+originalSeedPoints[j].GetIndex()[2]);
                dataSet << originalSeedPoints[j].GetIntensity();
                dataSet << originalSeedPoints[j].GetSeedPoint3D();
                dataSet << originalSeedPointCombinations[j];
                mMetaDataFilter->mData.append( dataSet );
            }

            ++numValidSeeds;
        }
    }

    // close the file handle
//    keyPointsFile.close();

    // display the number of combined nuclei
    if (mFuseSeedPoints == true){
        Logger::GetInstance()->WriteLine( "+ Combined (" + QString::number(originalSeedPoints.size()-numValidSeeds) + " / "
                                             +  QString::number(originalSeedPoints.size()) + ") detected seed points." );
    }
    // print success message
    Logger::GetInstance()->WriteLine( "+ ExtractKeyPointsImageFilter: Found " + QString::number(numValidSeeds) + " local maxima in the provided image." );

    // print the spacing of the investigated image
    for(int i = 0; i<TInputImage::ImageDimension; ++i)
        Logger::GetInstance()->Write( QString::number( spacing[i] ) + (i < (TInputImage::ImageDimension-1) ? ", " : "" ) );
    Logger::GetInstance()->WriteLine( "]" );

//    Logger::GetInstance()->WriteLine("+ Seed locations were transformed to physical space with the following spacing: [" +
//                                        QString::number(spacing[0]) + ", " + QString::number(spacing[1]) + ", " + QString::number(spacing[2]) + "]\n");
}

} // end namespace itk

#endif
