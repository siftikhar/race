/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifndef EXTRACTKEYPOINTSIMAGEFILTER_H
#define EXTRACTKEYPOINTSIMAGEFILTER_H

// include required headers
#include "../Base/Management/ITKDefinitions.h"
#include "itkBoxImageFilter.h"
#include "itkImage.h"
#include "itkNumericTraits.h"
#include "itkNeighborhoodIterator.h"
#include "itkConstNeighborhoodIterator.h"
#include "itkZeroFluxNeumannBoundaryCondition.h"
#include <vector>
#include <QString>

#include "../Base/MetaData/MetaDataFilter.h"

using namespace XPIWIT;

namespace itk
{

/**
 * @class SeedPoint container class for seed points.
 * used to store informatino such as index, scale, intensity and an id.
 */
template<class TInputImage>
class SeedPoint
{
public:
    typedef typename TInputImage::IndexType IndexType;
    SeedPoint(IndexType index, float scale, int id = 0, float intensity = 0.0, bool seedPoint3D = false) { m_Index = index; m_Scale = scale; m_ID = id; m_Intensity = intensity; m_SeedPoint3D = seedPoint3D; }
    ~SeedPoint() {}

    IndexType GetIndex() { return m_Index; }
    int GetID() { return m_ID; }
    void SetIndex(IndexType index) { m_Index = index; }
    float GetIntensity() { return m_Intensity; }
    float GetScale() { return m_Scale; }
    bool GetSeedPoint3D() { return m_SeedPoint3D; }
    void SetScale(float scale) { m_Scale = scale; }
    void SetID(int id) { m_ID = id; }
    void SetIntensity(float intensity) {m_Intensity = intensity; }
    void SetSeedPoint3D(bool seedPoint3D) { m_SeedPoint3D = seedPoint3D; }

private:
    IndexType m_Index;
    float m_Scale;
    float m_Intensity;
    int m_ID;
    bool m_SeedPoint3D;
};


/**
 * @class ExtractKeyPointsImageFilter extracts local maxima from a given image.
 */
template< class TInputImage, class TOutputImage, class TMaximumScaleImage >
class ITK_EXPORT ExtractKeyPointsImageFilter : public BoxImageFilter< TInputImage, TOutputImage >
{
public:
    // Extract dimension from input and output image.
    itkStaticConstMacro(InputImageDimension, unsigned int, TInputImage::ImageDimension);
    itkStaticConstMacro(OutputImageDimension, unsigned int, TOutputImage::ImageDimension);

    // Convenient typedefs for simplifying declarations.
    typedef TInputImage  InputImageType;
    typedef TOutputImage OutputImageType;
    typedef TMaximumScaleImage MaximumScaleImageType;

    // Standard class typedefs.
    typedef ExtractKeyPointsImageFilter                       Self;
    typedef BoxImageFilter< InputImageType, OutputImageType > Superclass;
    typedef SmartPointer< Self >                              Pointer;
    typedef SmartPointer< const Self >                        ConstPointer;

    // Method for creation through the object factory.
    itkNewMacro(Self);

    // Run-time type information (and related methods).
    itkTypeMacro(ExtractKeyPointsImageFilter, BoxImageFilter);

    // Image typedef support.
    typedef typename InputImageType::PixelType                 InputPixelType;
    typedef typename OutputImageType::PixelType                OutputPixelType;
    typedef typename NumericTraits< InputPixelType >::RealType InputRealType;
    typedef SeedPoint< InputImageType >				 SeedPointType;

    typedef typename InputImageType::RegionType  InputImageRegionType;
    typedef typename OutputImageType::RegionType OutputImageRegionType;

    typedef typename InputImageType::SizeType InputSizeType;
    typedef typename itk::ZeroFluxNeumannBoundaryCondition<InputImageType, InputImageType> ZeroFluxNeumannBoundaryConditionType;
    typedef itk::NeighborhoodIterator< InputImageType, ZeroFluxNeumannBoundaryConditionType> NeighborhoodIteratorType;
    typedef typename NeighborhoodIteratorType::IndexType NeighborhoodIteratorIndexType;

    /** Sets the maxiumum scale image if this information is required for keypoint generation */
    void SetMaximumScaleImage(const MaximumScaleImageType* image)               { mMaximumScaleImage = image; }

    /** Sets the filename for the keypoints file */
    void SetFileName(const QString& fileName)									{ mFileName = fileName; }

    /** Sets the keypoint threshold, i.e. keypoints below this value are omitted */
    void SetKeyPointThreshold( InputRealType threshold )						{ mThreshold = threshold; }

    /** Sets the quantile threshold, i.e. keypoints below this value are omitted */
    void SetKeyPointQuantile( InputRealType quantile )							{ mQuantile = quantile; }

    /** Sets the keypoint std dev multiplocator if mean based threshold is used. Defaults to 2 */
    void SetStdDevMultiplicator( InputRealType stdDevMultiplicator )			{ mStdDevMultiplicator = stdDevMultiplicator; }

    /** Set the keypoint offset, e.g. use 1 for MATLAB like indices starting from 1...numPixels. */
    void SetKeyPointOffset(const unsigned int keyPointOffset)					{ mKeyPointOffset = keyPointOffset; }

    /** Set the neighborhood to use in the case of 3 dimensions */
    void SetNeighborhood3D(const int neighborhood3D)							{ mNeighborhood3D = neighborhood3D; }

    /** Set the neighborhood radius */
    void SetNeighborhoodRadius(const int neighborhoodRadius)                    { mNeighborhoodRadius = neighborhoodRadius; }

    /** Set fuse seed points */
    void SetFuseSeedPoints(bool fuseSeedPoint)									{ mFuseSeedPoints = fuseSeedPoint; }

    /** Set minimum seed combinations */
    void SetMinimumSeedCombinations(const int minimumSeedCombinations)			{ mMinimumSeedCombinations = minimumSeedCombinations; }

    /** Set flag for to remove 2d seeds if they are intersecting the bounding sphere of a 3d seed */
    void SetRemove2DSeedsTouching3DSeeds(bool remove2DSeedsTouching3DSeeds)		{ mRemove2DSeedsTouching3DSeeds = remove2DSeedsTouching3DSeeds; }

    void SetAllowMaximumPlateaus(bool allowMaximumPlateaus)						{ mAllowMaximumPlateaus = allowMaximumPlateaus; }

    /** Set the Object for meta data handling */
    void SetMetaDataFilter( XPIWIT::MetaDataFilter* metaDataFilter )      { mMetaDataFilter = metaDataFilter; }

    /** calculates the threshold value based on the given quantile */
    float CalculateQuantile(const float quantile);

#ifdef ITK_USE_CONCEPT_CHECKING
    /** Begin concept checking */
    itkConceptMacro( InputHasNumericTraitsCheck,
                     ( Concept::HasNumericTraits< InputPixelType > ) );
    /** End concept checking */
#endif
protected:
    ExtractKeyPointsImageFilter();
    virtual ~ExtractKeyPointsImageFilter();

    /** ExtractKeyPointsImageFilter can be implemented as a multithreaded filter.
   * Therefore, this implementation provides a ThreadedGenerateData()
   * routine which is called for each processing thread. The output
   * image data is allocated automatically by the superclass prior to
   * calling ThreadedGenerateData().  ThreadedGenerateData can only
   * write to the portion of the output image specified by the
   * parameter "outputRegionForThread"
   *
   * \sa BoxImageFilter::ThreadedGenerateData(),
   *     BoxImageFilter::GenerateData() */
    void ThreadedGenerateData(const OutputImageRegionType & outputRegionForThread,
                              ThreadIdType threadId);


    /** functions for prepareing and post-processing the threaded generation */
    void BeforeThreadedGenerateData();
    void AfterThreadedGenerateData();

private:
    ExtractKeyPointsImageFilter(const Self &); //purposely not implemented
    void operator=(const Self &);  //purposely not implemented

    const MaximumScaleImageType*  mMaximumScaleImage; // pixel values represent the scale at which the maximum was detected

    InputRealType mThreshold; // keypoints below this value are omitted
    InputRealType mStdDevMultiplicator; // keypoints below mean+stdDevMultiplicator*stdDev value are omitted
    InputRealType mQuantile;
    std::vector<NeighborhoodIteratorIndexType>* mKeyPoints;
    std::vector<SeedPointType>* mSeedPoints3D;
    std::vector<SeedPointType>* mSeedPoints2D;
    int mNeighborhood3D; // if true, 26 neighbors are used for 3d images, else only the neighbors in the xy plane are used
    int mNeighborhoodRadius; // set the neighborhood radius. Defaults to 1
    QString mFileName; // filename to write the keypoints to
    unsigned int mKeyPointOffset; // offset for the keypoint indices. e.g. for matlab each keypoint should be increased by 1
    bool mFuseSeedPoints;
    bool mRemove2DSeedsTouching3DSeeds;
    bool mAllowMaximumPlateaus;
    int mMinimumSeedCombinations;
    XPIWIT::MetaDataFilter *mMetaDataFilter;
};

} // end namespace itk

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkExtractKeyPointsImageFilter.txx"
#endif

#endif
