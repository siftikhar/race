/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// project header
#include "CSVToSeedPointImageFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"
#include "../../ITKCustom/itkLabelImageToUncertaintyMapFilter.h"


namespace XPIWIT
{

// the default constructor
template< class TInputImage >
CSVToSeedPointImageFilterWidget<TInputImage>::CSVToSeedPointImageFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = CSVToSeedPointImageFilterWidget<TInputImage>::GetName();
	this->mDescription = "Converts the spatial coordinates into an empty image of the size of the provided image. Uncertainty map can be used to only use selected locations.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
    this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(1);
	this->mObjectType->AppendMetaInputType("RegionProps");
    this->mObjectType->SetNumberMetaOutputs(0);
	
    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "LabelOutput", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If true, the filter directly produces a labeled output image with a unique id for each detected blob." );
	processObjectSettings->AddSetting( "UseImageSpacing", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If true, the filter divides seed locations by the specified image spacing." );
	processObjectSettings->AddSetting( "LabelOffset", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Added to the original seed label. e.g. useful to avoid first seed to have the background label." );
	processObjectSettings->AddSetting( "UncertaintyThreshold", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Considers only seed points with intensity values below the threshold. An uncertainty map is required as input image." );
	processObjectSettings->AddSetting( "BackgroundLabel", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Skips the background label which usually contains negative indices and should not be added as a seed." );

	/*
    processObjectSettings->AddSetting( "MinimumRegionSigma", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The minimum std. dev. within a cropped image region. Ignored in the current implementation." );
    processObjectSettings->AddSetting( "Segment3D", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Used to perform the segmentation either directly in 3D or to merge 2D segmentation results instead." );
    
    processObjectSettings->AddSetting( "RandomLabels", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If checked, random labels are used. Note that random labels might not be unique." );
    processObjectSettings->AddSetting( "WriteRegionProps", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If checked, the region props of extracted blobs are exported to a cvs file." );
    processObjectSettings->AddSetting( "MinimumWeightedGradientNormalDotProduct", "0.6", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Used to threshold the weighted dot product image." );
    processObjectSettings->AddSetting( "WeightingKernelSizeMultiplicator", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Used to scale the plateau region of the weighting kernel. If set to 1 the seed radius is used for the plateau radius." );
    processObjectSettings->AddSetting( "WeightingKernelStdDev", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Multiplier for the weighting kernel standard deviation." );
    processObjectSettings->AddSetting( "GradientImageStdDev", "1.5", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The standard deviation of the Gaussian smoothing for smoother gradient directions." );
	*/

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
CSVToSeedPointImageFilterWidget<TInputImage>::~CSVToSeedPointImageFilterWidget()
{
}


// the update function
template< class TInputImage >
void CSVToSeedPointImageFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	const bool labelOutput = processObjectSettings->GetSettingValue( "LabelOutput" ).toInt() > 0;
	const int labelOffset = processObjectSettings->GetSettingValue( "LabelOffset" ).toInt();
	const bool useImageSpacing = processObjectSettings->GetSettingValue( "UseImageSpacing" ).toInt() > 0;
	const float uncertaintyThreshold = processObjectSettings->GetSettingValue( "UncertaintyThreshold" ).toFloat();
	const int backgroundLabel= processObjectSettings->GetSettingValue( "BackgroundLabel" ).toInt();

	MetaDataFilter* regionProps = this->mMetaInputs.at(0);

	// initialize the output
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	typename TInputImage::Pointer outputImage = TInputImage::New();
	outputImage->SetRegions( inputImage->GetLargestPossibleRegion() );
	outputImage->SetSpacing( inputImage->GetSpacing() );
	outputImage->SetOrigin( inputImage->GetOrigin() );
	outputImage->Allocate();
	outputImage->FillBuffer(0);

	// calculate the fuzzy set membership values for each of the objects
	typename TInputImage::IndexType index;
	typename TInputImage::SpacingType spacing = inputImage->GetSpacing();
	const unsigned int numObjects = regionProps->mData.length();
	for (int i=0; i<numObjects; ++i)
	{
		// skip the background label
//		if (regionProps->mData[i][0] == backgroundLabel)
//			continue;

		if (useImageSpacing == true)
		{
			index[0] = (unsigned int)(regionProps->mData[i][2] / spacing[0]);
			index[1] = (unsigned int)(regionProps->mData[i][3] / spacing[1]);
			index[2] = (unsigned int)(regionProps->mData[i][4] / spacing[2]);
		}
		else
		{
			index[0] = (unsigned int)(regionProps->mData[i][2]);
			index[1] = (unsigned int)(regionProps->mData[i][3]);
			index[2] = (unsigned int)(regionProps->mData[i][4]);
		}

		float currValue = inputImage->GetPixel(index);
		if (inputImage->GetPixel(index) <= uncertaintyThreshold)
		{
			if (labelOutput == true)
				outputImage->SetPixel(index, i+labelOffset);
			else
				outputImage->SetPixel(index, 1);
		}
	}
	outputImage->SetReleaseDataFlag( false );

	// set the output image   
	ImageWrapper *outputImageWrapper = new ImageWrapper();
    outputImageWrapper->SetImage<TInputImage>( outputImage );
	if (labelOutput == true)
		outputImageWrapper->SetRescaleFlag( false );
    mOutputImages.append( outputImageWrapper );

    // update the base class
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< CSVToSeedPointImageFilterWidget<Image2Float> > CSVToSeedPointImageFilterWidgetImage2Float;
static ProcessObjectProxy< CSVToSeedPointImageFilterWidget<Image3Float> > CSVToSeedPointImageFilterWidgetImage3Float;
static ProcessObjectProxy< CSVToSeedPointImageFilterWidget<Image2UShort> > CSVToSeedPointImageFilterWidgetImage2UShort;
static ProcessObjectProxy< CSVToSeedPointImageFilterWidget<Image3UShort> > CSVToSeedPointImageFilterWidgetImage3UShort;

} // namespace XPIWIT
