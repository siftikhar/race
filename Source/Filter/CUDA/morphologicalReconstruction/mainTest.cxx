/**
 * Efficient Membrane Segmentation in Large-Scale Microscopy Images.
 * Copyright (C) 2014 J. Stegmaier, F. Amat, A. Bartschat, P. J. Keller, R. Mikut and Howard Hughes Medical Institute
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, R. Mikut, P. J. Keller: 
 * "Efficient membrane segmentation in large-scale microscopy images", submitted manuscript, 2014.
 */

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string.h>
#include "external/Nathan/tictoc.h"
#include "morphologicalReconstruction.h"


typedef unsigned short imageType;//define here any type you want

using namespace std;

int writeImage(void* im,long long int imSizeBytes,const char* filename)
{
	FILE* fid = fopen(filename,"wb");
	if(fid == NULL)
	{
		cout<<"ERROR: at writeImage opening file "<<filename<<endl;
		return 1;
	}
	cout<<"Writing file "<<filename<<endl;
	fwrite(im,sizeof(char),imSizeBytes,fid);
	fclose(fid);

	return 0;
}

int main( int argc, const char** argv )
{

	cout<<"=============DEBUGGING:morphologicalReconstruction using CUDA =================="<<endl;
	int devCUDA = 0;
	int conn3D = 8;
	int numCalls = 1;//in case process is too fast
	
	//easy synthetic image
	/*
	string filename("E:/temp/testWatershed/syntheticIm.bin");
	std::int64_t imgDims[dimsImage] = {32, 63, 40};
	unsigned short int levelH = 100;
	*/

	//easy 2D synthetic image
	/*
	string filename("E:/temp/testWatershed/syntheticIm2D.bin");
	std::int64_t imgDims[dimsImage] = {32, 63, 1};
	unsigned short int levelH = 100;
	conn3D = 8;
	numCalls = 100;
	*/

	//real zebrafish image cropped	
	/*
	string filename("E:/temp/testWatershed/imCrop.bin");
	std::int64_t imgDims[dimsImage] = {101,   151,    41};
	unsigned short int levelH = 20;	
	*/

	//real zebrafish not cropped (large dataset)	
	
	string filename("E:/temp/testWatershed/im.bin");
	std::int64_t imgDims[dimsImage] = { 1818 ,       1792 ,        30};//we reduce planes because malloc crashes in WIn32 mode
	unsigned short int levelH = 20;	
	
	
	//---------------------------------------
	TicTocTimer tt = tic();

	std::int64_t imSize = 1;
	for(int ii = 0; ii < dimsImage; ii++)
		imSize *= imgDims[ii];

	//read binary stream
	ifstream imIn(filename.c_str(), ios::binary | ios::in );
	unsigned short int *img = new unsigned short int[imSize];
	imIn.read((char*)img, sizeof(short unsigned int) * imSize );

	

	//declare class
	morphologicalReconstruction_CUDA<short unsigned int> wPBC(img, imgDims, conn3D, devCUDA);

	for(int ii = 0; ii < numCalls; ii++)
	{
		int err = wPBC.hMaximaFilter(levelH);
		if( err > 0 )
			return err;	
	}	

	
	//------debugging: check result---------------------
	cout<<"==========DEBUGGING: writing result to E:/temp/testWatershed/imCrop_L.bin in uin32 with size "<<imgDims[0]<<"x"<<imgDims[1]<<"x"<<imgDims[2]<<endl;	
	wPBC.getImageFromGPU(img);

	cout<<"Total time per call took "<<toc(&tt) / (float)numCalls<<endl;
	
	ofstream out( "E:/temp/testWatershed/imCrop_L.bin", ios::binary | ios::out );
	out.write((char*)img, sizeof(unsigned short int) * wPBC.numElements());	
	out.close();
	//---------------------------------------------------



	//release memory
	delete[] img;
	return 0;
}
