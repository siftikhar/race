/**
 * Efficient Membrane Segmentation in Large-Scale Microscopy Images.
 * Copyright (C) 2014 J. Stegmaier, F. Amat, A. Bartschat, P. J. Keller, R. Mikut and Howard Hughes Medical Institute
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, R. Mikut, P. J. Keller: 
 * "Efficient membrane segmentation in large-scale microscopy images", submitted manuscript, 2014.
 */
 
 /**
  * \brief Code to calculate 2D median filter in CUDA using templates and different window sizes
  * \Note: this code can be easily used for any non-linear processing in a neighborhood block around each pixel with a simple modifcation. Analogous to blockproc by Matlab
  */

#ifndef __WATERSHED_PBC_CUDA_H__
#define __WATERSHED_PBC_CUDA_H__


#include <vector>

//define constants
typedef unsigned long long uint64;
typedef long long int64;

typedef unsigned int imgLabelTypeCUDA;//decide the format of the segmentation mask (it defines the maximum number of labels allowed)
static const  uint64 maxNumLabelsCUDA = 4294967295;//maximum number of labels. Label = 0 is always reserved for background

#ifndef DIMS_IMAGE_CONST //to protect agains teh same constant define in other places in the code
#define DIMS_IMAGE_CONST
	const static int dimsImage = 3;//to be able to precompile code
#endif



#ifndef CUDA_CONSTANTS_FA
#define CUDA_CONSTANTS_FA
static const int MAX_THREADS_CUDA_WPBC = 512; //maximum number of threads per block desired for thi sapplication. With 1024, we achieve 66% occupancy. With 512 we achieve 100%
static const int MAX_BLOCKS_CUDA = 65535; //maximum number of blocks
#endif


//================================================================================

/*
	\brief Main class containing all the methods to calculaye watershed + Persistence Based CLustering (PBC) in the GPU
	
*/
template<class imgType> 
class watershedPBC_CUDA
{
public:
	
	std::vector<int64> foregroundVec;//index of the foreground elements

	//constructors
	watershedPBC_CUDA();
	~watershedPBC_CUDA();//destructor does not deallocate memory pointer to image (but it deallocates all the CUDA elements)
	watershedPBC_CUDA(imgType* img_, int64 *imgDims_, int conn3D_, int devCUDA);
			
	void initializeGPU(int devCUDA);
	
	
	//short inline functions
	int64 numElements();
	size_t numElementsForeground(){ return foregroundVec.size();};
	long long int* buildNeighboorhoodConnectivity(int *xyzOffset);
	void getForegroundLabelsFromGPU(imgLabelTypeCUDA* L);
	
	template<class maskType>
	int waterhsedWithSortedElementsNoTau(imgType backgroundThr, maskType* mask);//if mask == NULL -> then the backgroundThr is used. Otherwise, the backgthr is ignored.


	//-----------------------------debugging functions--------------------------------
	void debugSortPerBlock(int devCUDA);
	


protected:

private:
	int64 imgDims[dimsImage];//image size
	int conn3D;//connectivity to use for watershed
	imgType* img;//pointer to the image
		

	//CUDA variables
	imgLabelTypeCUDA *L_CUDA; //contains the label for each foreground index

};

template<class imgType>
inline int64 watershedPBC_CUDA<imgType>::numElements()
{
	int64 size = 1;
	for(int ii = 0; ii < dimsImage; ii++)
		size *= imgDims[ii];
	return size;
}

#endif //__WATERSHED_PBC_CUDA_H__