/**
 * Efficient Membrane Segmentation in Large-Scale Microscopy Images.
 * Copyright (C) 2014 J. Stegmaier, F. Amat, A. Bartschat, P. J. Keller, R. Mikut and Howard Hughes Medical Institute
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, R. Mikut, P. J. Keller: 
 * "Efficient membrane segmentation in large-scale microscopy images", submitted manuscript, 2014.
 */

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string.h>
#include <algorithm>
#include "external/Nathan/tictoc.h"
#include "watershedPBC.h"


#define USE_MASK_WATERSHED

typedef unsigned short imageType;//define here any type you want

using namespace std;

int writeImage(void* im,long long int imSizeBytes,const char* filename)
{
	FILE* fid = fopen(filename,"wb");
	if(fid == NULL)
	{
		cout<<"ERROR: at writeImage opening file "<<filename<<endl;
		return 1;
	}
	cout<<"Writing file "<<filename<<endl;
	fwrite(im,sizeof(char),imSizeBytes,fid);
	fclose(fid);

	return 0;
}

int main( int argc, const char** argv )
{

	cout<<"=============DEBUGGING:watershedPersistanceAgglomeration using CUDA =================="<<endl;
	int devCUDA = 0;
	int conn3D = 74;
	int numCalls = 1;//in case process is too fast
	
	//easy synthetic image
	/*
	string filename("E:/temp/testWatershed/syntheticIm.bin");
	int64 imgDims[dimsImage] = {32, 63, 40};
	unsigned short int backgroundThr = 100;
	*/

	//easy 2D synthetic image
	/*
	string filename("E:/temp/testWatershed/syntheticIm2D.bin");
	int64 imgDims[dimsImage] = {32, 63, 1};
	unsigned short int backgroundThr = 100;
	conn3D = 8;
	numCalls = 100;
	*/

	//2D image
	/*
	string filename("E:/temp/testXPIWIT/watershedInput_slice14.bin");
	int64 imgDims[dimsImage] = {51, 121, 1};
	unsigned short int backgroundThr = 0;
	conn3D = 4;
	*/

	//3D image with a lot of NM regions
	string filename("E:/temp/testXPIWIT/watershedInput_plateau.bin");
	int64 imgDims[dimsImage] = {424, 284, 111};
	unsigned short int backgroundThr = 0;
	conn3D = 4;

	//real zebrafish image cropped	
	/*
	string filename("E:/temp/testWatershed/imCrop.bin");
	int64 imgDims[dimsImage] = {101,   151,    41};
	unsigned short int backgroundThr = 20;	
	*/

	//real zebrafish not cropped (large dataset)		
	//string filename("E:/temp/testWatershed/im.bin");
	//int64 imgDims[dimsImage] = { 1818 ,       1792 ,        30};//we reduce planes because malloc crashes in WIn32 mode
	//unsigned short int backgroundThr = 20;	
		
	
	//---------------------------------------
	
	int64 imSize = 1;
	for(int ii = 0; ii < dimsImage; ii++)
	{
		imSize *= imgDims[ii];
	}

	//read binary stream
	ifstream imIn(filename.c_str(), ios::binary | ios::in );
	unsigned short int *img = new unsigned short int[imSize];
	imIn.read((char*)img, sizeof(short unsigned int) * imSize );
	imIn.close();
	
#ifdef USE_MASK_WATERSHED
	//read mask
	imIn.open("E:/temp/testXPIWIT/morphologicalOp_mask.bin", ios::binary | ios::in );
	unsigned short int *mask = new unsigned short int[imSize];
	imIn.read((char*)mask, sizeof(short unsigned int) * imSize );
	imIn.close();

#endif
	

	//------------start watershed----------------------------
	TicTocTimer tt = tic();


#ifdef USE_MASK_WATERSHED

	//restrict slices that are only background
	int64 imIdx = 0;
	unsigned short int bb = img[imIdx];
	while( img[imIdx] == bb )
		imIdx++;
	int iniSlice = floor (float(imIdx) / float(imgDims[0] * imgDims[1]));

	imIdx = imSize - 1;
	bb = img[imIdx];
	while( img[imIdx] == bb )
		imIdx--;
	int endSlice = imgDims[2] - floor (float(imSize - 1 - imIdx) / float(imgDims[0] * imgDims[1]));

	//add these slices to mask
	imIdx = 0;
	for(int64 slice = 0; slice < iniSlice; slice++ )
	{
		for(int idx = 0; idx < imgDims[0] * imgDims[1]; idx++, imIdx++)
		{
			mask[imIdx] = 0;
		}
	}
	imIdx = endSlice * imgDims[0] * imgDims[1];
	for(int64 slice = endSlice; slice < imgDims[2]; slice++ )
	{
		for(int idx = 0; idx < imgDims[0] * imgDims[1]; idx++, imIdx++)
		{
			mask[imIdx] = 0;
		}
	}
	cout<<"Inislice = "<<iniSlice<<";endSlice="<<endSlice<<endl;


	tt = tic();

	/*
	imgDims[2] = 1;
	for(int slice = iniSlice; slice < endSlice; slice++)
	{
		cout<<"Watershed in slice "<<slice<<endl;
		watershedPBC_CUDA<short unsigned int> wPBCslice(&(img[slice * imgDims[0] * imgDims[1]]), imgDims, conn3D, devCUDA);
		wPBCslice.waterhsedWithSortedElementsNoTau(backgroundThr, &(mask[slice * imgDims[0] * imgDims[1]]));
	}
	*/
	//declare class
	watershedPBC_CUDA<short unsigned int> wPBC(img, imgDims, conn3D, devCUDA);

	for(int ii = 0; ii < numCalls; ii++)
	{
		int err = wPBC.waterhsedWithSortedElementsNoTau(backgroundThr, mask);
		if( err > 0 )
			return err;	
	}

	
	

#elif
	//declare class
	watershedPBC_CUDA<short unsigned int> wPBC(img, imgDims, conn3D, devCUDA);

	for(int ii = 0; ii < numCalls; ii++)
	{
		int err = wPBC.waterhsedWithSortedElementsNoTau<std::uint8_t>(backgroundThr, NULL);
		if( err > 0 )
			return err;	
	}	
#endif
	
	//------debugging: check result---------------------
	cout<<"==========DEBUGGING: writing result to E:/temp/testXPIWIT/watershedOutput.bin in uint32 with size "<<imgDims[0]<<"x"<<imgDims[1]<<"x"<<imgDims[2]<<endl;
	imgLabelTypeCUDA* L = new imgLabelTypeCUDA[imSize];
	int64 Nf = wPBC.numElementsForeground();
	imgLabelTypeCUDA* Lf = new imgLabelTypeCUDA[Nf];
	wPBC.getForegroundLabelsFromGPU(Lf);

	cout<<"Total time per call took "<<toc(&tt) / (float)numCalls<<endl;

	memset(L,0,imSize * sizeof(imgLabelTypeCUDA));
	imgLabelTypeCUDA maxL = 0;
	for(size_t ii = 0; ii < Nf; ii++)
	{
		L[ wPBC.foregroundVec[ii] ] = Lf[ii];
		maxL = std::max(maxL, Lf[ii]);
	}

	//parse labels to consecutive labels
	if( imgDims[2] > 1 && (conn3D == 4 || conn3D == 8))
	{
		int64 count = 0;
		short unsigned int *mapLabel = new short unsigned int[maxL + 1];//TODO amatf: use an efficient Hash table (VS2010 std::unordered_map is not)
		memset(mapLabel, 0, sizeof(short unsigned int) * (maxL + 1));
		for(int64 slice = 0; slice < imgDims[2]; slice++ )
		{
			short unsigned int numLabels = 0, label;
			for(int idx = 0; idx < imgDims[0] * imgDims[1]; idx++, count++)
			{
				if( L[count] != 0 )//L[count] = 0 -> background
				{
					label = mapLabel[ L[count] ];
					if( label == 0 )
					{
						numLabels++;
						label = numLabels;
						mapLabel[ L[count] ] = label;
					}				
					L[count] = label;
				}
			}
		}
		delete[] mapLabel;		
	}else{
		
		int *mapLabel = new int[maxL + 1];//TODO amatf: use an efficient Hash table (VS2010 std::unordered_map is not)
		memset(mapLabel, 0, sizeof(int) * (maxL + 1));
		int numLabels = 0, label;
		for(int64 count = 0; count <  wPBC.numElements(); count++ )
		{
			if( L[count] != 0 )//L[count] = 0 -> background
			{
				label = mapLabel[ L[count] ];
				if( label == 0 )
				{
					numLabels++;
					label = numLabels;
					mapLabel[ L[count] ] = label;
				}
				L[count] = label;			
			}
		}
		delete[] mapLabel;
	}	

	ofstream out( "E:/temp/testXPIWIT/watershedOutput.bin", ios::binary | ios::out );
	out.write((char*)L, sizeof(imgLabelTypeCUDA) * imSize);
	delete[] L;
	delete[] Lf;
	//---------------------------------------------------



	//release memory
#ifdef USE_MASK_WATERSHED
	delete[] mask;
#endif
	delete[] img;
	return 0;
}
