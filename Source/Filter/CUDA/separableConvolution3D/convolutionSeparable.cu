/*
 * Copyright 1993-2014 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */



#include <assert.h>
#include <iostream>
#include "convolutionSeparable_common.h"
#include "book.h"
#include "cuda.h"

////////////////////////////////////////////////////////////////////////////////
// Convolution kernel storage
////////////////////////////////////////////////////////////////////////////////
__constant__ float c_Kernel[MAX_KERNEL_LENGTH];

__constant__ std::int64_t imgDimsCUDA[dimsImage];//stores number of elements along each of the image's dimensions


//=======================================================================================================
//each grop of threads within a block calculates the values of convolution in a whole column along the direction of the separable kernel. It does not provide coalescent memory access along Y and Z axis but we can use the shared memory to store all the column at once
template<int sharedMemoryUbSize, int numImgDims>
__global__ void __launch_bounds__(MAX_THREADS_CUDA_SCONV) separableConvolutionInplaceKernel(float* volCUDA, const int kernelRadius, const int dim, const int imgSize)
{
	
	__shared__ float colConv[sharedMemoryUbSize];
	__shared__ float colOrig[sharedMemoryUbSize];//so we do not overwrite values while computing convolution	


	int blockIdxPos = blockIdx.x;			

	while( blockIdxPos < imgSize / imgDimsCUDA[dim] )
	{
		int iniPosXYZ [numImgDims];
		int offset = (dim+1)%numImgDims;
		iniPosXYZ[dim] = 0; //the initial pos is a plane of dim-th dimension = 0
		iniPosXYZ[offset] = blockIdxPos % imgDimsCUDA[offset];// generate a grid withthe other dimensions
		iniPosXYZ[(dim+2)%numImgDims] = (blockIdxPos -iniPosXYZ[offset])/ imgDimsCUDA[offset]; 

		int posOrig = 0;
		offset = 1;
		for(int ii=0;ii<numImgDims;ii++)
		{
			posOrig += offset * iniPosXYZ[ii];
			offset *= imgDimsCUDA[ii];
		}

		int stride = 1;//how much do we have to skip to find next value along the separable kernel axis
		for(int ii=0;ii<dim;ii++)//for ii=0->offset=1->coalescent memory access
			stride *= imgDimsCUDA[ii];

		//reset values	along all the line in the image	
		int posLine = threadIdx.x;
		int pos = posOrig + stride * posLine;
		while( posLine < imgDimsCUDA[dim] )
		{
			colOrig[posLine] = volCUDA[pos];
			colConv[posLine] = 0;

			posLine += blockDim.x;
			pos = posOrig + stride * posLine;
		}	

		__syncthreads();

		//calculate value of each pixel
		posLine = threadIdx.x;
		while( posLine < imgDimsCUDA[dim] )
		{
			offset = posLine - kernelRadius;
			float auxVal;
			for(int ii = 2* kernelRadius;ii >= 0;ii--)//we need to flip the kernel for convolution
			{
				if(offset<0) //make sure we are within bounds. TODO: allow different boundary conditions.
					auxVal = colOrig[0]; //replicate the first and last value 
					//auxVal = 0; //zero padding (seems to be the default for convn in Matlab)
				else if(offset>= imgDimsCUDA[dim])
					auxVal = colOrig[imgDimsCUDA[dim]-1];
					//auxVal = 0;
				else
					auxVal = colOrig[offset];

				colConv[posLine] += auxVal * c_Kernel[ii];

				offset++;
			}	

			posLine += blockDim.x;
			__syncthreads();
		}


		//copy result back to global memory
		posLine = threadIdx.x;
		pos = posOrig + stride * posLine;
		while( posLine < imgDimsCUDA[dim] )
		{
			volCUDA[pos] = colConv[posLine];

			posLine += blockDim.x;
			pos = posOrig + stride * posLine;
		}

		blockIdxPos += gridDim.x;//number of blocks
	}
}



//=============================================================
template<class imageType>
__global__ void parseImgToFloat (float *d_Dst, const imageType *d_Src, std::int64_t imSize)
{
	std::int64_t tid = blockIdx.x * blockDim.x + threadIdx.x;
	while( tid < imSize )
	{
		d_Dst[tid] = (float) (d_Src[tid]);
		tid += (long long int ) (blockDim.x * gridDim.x);	
	}

}

template<class imageType>
__global__ void parseFloatToImage (imageType *d_Dst, const float *d_Src, std::int64_t imSize)
{
	std::int64_t tid = blockIdx.x * blockDim.x + threadIdx.x;
	while( tid < imSize )
	{
		d_Dst[tid] = (imageType)(d_Src[tid]);
		tid += (long long int ) (blockDim.x * gridDim.x);	
	}

}


//========================================================================

template<class imgType>
separableConvolution_CUDA<imgType>::separableConvolution_CUDA()
{
	img = NULL;
	memset(imgDims, 0 , sizeof(std::int64_t) * dimsImage );

	img_CUDA = NULL;
	img_CUDAbuffer = NULL;
};

template<class imgType>
separableConvolution_CUDA<imgType>::~separableConvolution_CUDA()
{
	//std::cout<<"DEBUGGING: separableConvolution_CUDA<imgType>::~separableConvolution_CUDA() is being called to deallocate CUDA elements"<<std::endl;
	//deallocate cuda elements	
	if( img_CUDA != NULL )
	{
		HANDLE_ERROR( cudaFree( img_CUDA ) );
	}
	if( img_CUDAbuffer != NULL )
	{
		HANDLE_ERROR( cudaFree( img_CUDAbuffer ) );
	}
};

template<class imgType>
separableConvolution_CUDA<imgType>::separableConvolution_CUDA(imgType* img_, std::int64_t *imgDims_, int devCUDA)
{
	img = img_;
	memcpy(imgDims, imgDims_, sizeof(std::int64_t) * dimsImage );

	initializeGPU(devCUDA);
};


template<class imgType>
void separableConvolution_CUDA<imgType>::initializeGPU(int devCUDA)
{

	HANDLE_ERROR( cudaSetDevice( devCUDA ) );

	//copy image dimensions to GPU
	cudaMemcpyToSymbol(imgDimsCUDA, imgDims, dimsImage * sizeof(std::int64_t) );
		
	//allocate memory for image	
	std::int64_t imSize = numElements();

	HANDLE_ERROR(cudaMalloc((void**)&(img_CUDA), imSize * sizeof(imgType)));
	HANDLE_ERROR(cudaMalloc((void**)&(img_CUDAbuffer), imSize * sizeof(float)));

	//copy image to GPU
	HANDLE_ERROR(cudaMemcpy(img_CUDA, img, imSize * sizeof(imgType), cudaMemcpyHostToDevice));


	//convert into float
	int numBlocks_Img = std::min( (int) ceil(((float)imSize) / (float) MAX_THREADS_CUDA_SCONV), MAX_BLOCKS_CUDA);
	parseImgToFloat<imgType><<<numBlocks_Img,MAX_THREADS_CUDA_SCONV>>> (img_CUDAbuffer, img_CUDA, imSize); HANDLE_ERROR_KERNEL;

	//deallocate image memory
	HANDLE_ERROR( cudaFree(img_CUDA) );
	img_CUDA = NULL;	

}

//special case when imgType is already float
template<>
void separableConvolution_CUDA<float>::initializeGPU(int devCUDA)
{
	HANDLE_ERROR( cudaSetDevice( devCUDA ) );
		
	//allocate memory for image	
	std::int64_t imSize = numElements();

	HANDLE_ERROR(cudaMalloc((void**)&(img_CUDAbuffer), imSize * sizeof(float)));

	//copy image to GPU
	HANDLE_ERROR(cudaMemcpy(img_CUDAbuffer, img, imSize * sizeof(float), cudaMemcpyHostToDevice));

	img_CUDA = NULL;

}

//=========================================================================
template<class imgType>
void separableConvolution_CUDA<imgType>::getImageFromGPU(imgType* img_)
{

	std::int64_t imSize = numElements();
	//allocate memory for img_CUDA
	if( img_CUDA == NULL )
		HANDLE_ERROR(cudaMalloc((void**)&(img_CUDA), imSize * sizeof(imgType)));


	int numBlocks_Img = std::min( (int) ceil(((float)imSize) / (float) MAX_THREADS_CUDA_SCONV), MAX_BLOCKS_CUDA);
	parseFloatToImage<imgType><<<numBlocks_Img,MAX_THREADS_CUDA_SCONV>>> (img_CUDA, img_CUDAbuffer, imSize);HANDLE_ERROR_KERNEL;

	if( img_ == NULL )//overwrite *img
	{
		HANDLE_ERROR(cudaMemcpy(img, img_CUDA, numElements() * sizeof(imgType), cudaMemcpyDeviceToHost));
	}else{
		HANDLE_ERROR(cudaMemcpy(img_, img_CUDA, numElements() * sizeof(imgType), cudaMemcpyDeviceToHost));
	}
}

//special case when imgType = float
template<>
void separableConvolution_CUDA<float>::getImageFromGPU(float* img_)
{	
	if( img_ == NULL )//overwrite *img
	{
		HANDLE_ERROR(cudaMemcpy(img, img_CUDAbuffer, numElements() * sizeof(float), cudaMemcpyDeviceToHost));
	}else{
		HANDLE_ERROR(cudaMemcpy(img_, img_CUDAbuffer, numElements() * sizeof(float), cudaMemcpyDeviceToHost));
	}
}

template<class imgType>
void separableConvolution_CUDA<imgType>::updateKernel(const std::vector<float>& h_Kernel)	
{
    HANDLE_ERROR( cudaMemcpyToSymbol(c_Kernel, &(h_Kernel[0]), h_Kernel.size() * sizeof(float)) );	
	assert( h_Kernel.size() % 2 == 1);//it has ot be odd length
	kernel_radius = (h_Kernel.size() - 1)/2;
}

template<class imgType>
void separableConvolution_CUDA<imgType>::updateImageDimensionConstant(const std::int64_t imgDims_[dimsImage])	
{    
	HANDLE_ERROR( cudaMemcpyToSymbol(imgDimsCUDA, imgDims_, dimsImage * sizeof(std::int64_t) ) );
}



////////////////////////////////////////////////////////////////////////////////
// Row convolution filter
////////////////////////////////////////////////////////////////////////////////
#define   ROWS_BLOCKDIM_X 16
#define   ROWS_BLOCKDIM_Y 4
#define   ROWS_BLOCKDIM_Z 4
#define ROWS_RESULT_STEPS 8
#define   ROWS_HALO_STEPS 1

//TODO amatf: extend to images larger than 2GB by changing everything from int to std::int64_t
template<int kernelRadius>
__global__ void convolutionRowsKernel(float *d_Dst, const float *d_Src, int imageW, int imageH, int imageD)
{
    __shared__ float s_Data[ROWS_BLOCKDIM_Z][ROWS_BLOCKDIM_Y][(ROWS_RESULT_STEPS + 2 * ROWS_HALO_STEPS) * ROWS_BLOCKDIM_X];

    //Offset to the left halo edge
    const int baseX = (blockIdx.x * ROWS_RESULT_STEPS - ROWS_HALO_STEPS) * ROWS_BLOCKDIM_X + threadIdx.x;
    const int baseY = blockIdx.y * ROWS_BLOCKDIM_Y + threadIdx.y;
	const int baseZ = blockIdx.z * ROWS_BLOCKDIM_Z + threadIdx.z;

    d_Src += ((baseZ * imageH + baseY ) * imageW) + baseX;//resetting image pointer "origin" for EACH thread!!!
    d_Dst += ((baseZ * imageH + baseY ) * imageW) + baseX;

    //Load main data
#pragma unroll

    for (int i = ROWS_HALO_STEPS; i < ROWS_HALO_STEPS + ROWS_RESULT_STEPS; i++)
    {
        s_Data[threadIdx.z][threadIdx.y][threadIdx.x + i * ROWS_BLOCKDIM_X] = d_Src[i * ROWS_BLOCKDIM_X];
    }

    //Load left halo
#pragma unroll

    for (int i = 0; i < ROWS_HALO_STEPS; i++)
    {
        s_Data[threadIdx.z][threadIdx.y][threadIdx.x + i * ROWS_BLOCKDIM_X] = (baseX >= -i * ROWS_BLOCKDIM_X) ? d_Src[i * ROWS_BLOCKDIM_X] : 0;
    }

    //Load right halo
#pragma unroll

    for (int i = ROWS_HALO_STEPS + ROWS_RESULT_STEPS; i < ROWS_HALO_STEPS + ROWS_RESULT_STEPS + ROWS_HALO_STEPS; i++)
    {
        s_Data[threadIdx.z][threadIdx.y][threadIdx.x + i * ROWS_BLOCKDIM_X] = (imageW - baseX > i * ROWS_BLOCKDIM_X) ? d_Src[i * ROWS_BLOCKDIM_X] : 0;
    }

    //Compute and store results
    __syncthreads();
#pragma unroll

    for (int i = ROWS_HALO_STEPS; i < ROWS_HALO_STEPS + ROWS_RESULT_STEPS; i++)
    {
        float sum = 0;

#pragma unroll

        for (int j = -kernelRadius; j <= kernelRadius; j++)
        {
            sum += c_Kernel[kernelRadius - j] * s_Data[threadIdx.z][threadIdx.y][threadIdx.x + i * ROWS_BLOCKDIM_X + j];
        }

        d_Dst[i * ROWS_BLOCKDIM_X] = sum;
    }
}


////////////////////////////////////////////////////////////////////////////////
// Column convolution filter
////////////////////////////////////////////////////////////////////////////////
#define   COLUMNS_BLOCKDIM_X 16
#define   COLUMNS_BLOCKDIM_Y 8
#define COLUMNS_RESULT_STEPS 8
#define   COLUMNS_HALO_STEPS 1

template<int kernelRadius>
__global__ void convolutionColumnsKernel(float *d_Dst, float *d_Src, int imageW, int imageH, int imageD, int pitch)
{
    __shared__ float s_Data[COLUMNS_BLOCKDIM_X][(COLUMNS_RESULT_STEPS + 2 * COLUMNS_HALO_STEPS) * COLUMNS_BLOCKDIM_Y + 1];

    //Offset to the upper halo edge
    const int baseX = blockIdx.x * COLUMNS_BLOCKDIM_X + threadIdx.x;
    const int baseY = (blockIdx.y * COLUMNS_RESULT_STEPS - COLUMNS_HALO_STEPS) * COLUMNS_BLOCKDIM_Y + threadIdx.y;
    d_Src += baseY * pitch + baseX;
    d_Dst += baseY * pitch + baseX;

    //Main data
#pragma unroll

    for (int i = COLUMNS_HALO_STEPS; i < COLUMNS_HALO_STEPS + COLUMNS_RESULT_STEPS; i++)
    {
        s_Data[threadIdx.x][threadIdx.y + i * COLUMNS_BLOCKDIM_Y] = d_Src[i * COLUMNS_BLOCKDIM_Y * pitch];
    }

    //Upper halo
#pragma unroll

    for (int i = 0; i < COLUMNS_HALO_STEPS; i++)
    {
        s_Data[threadIdx.x][threadIdx.y + i * COLUMNS_BLOCKDIM_Y] = (baseY >= -i * COLUMNS_BLOCKDIM_Y) ? d_Src[i * COLUMNS_BLOCKDIM_Y * pitch] : 0;
    }

    //Lower halo
#pragma unroll

    for (int i = COLUMNS_HALO_STEPS + COLUMNS_RESULT_STEPS; i < COLUMNS_HALO_STEPS + COLUMNS_RESULT_STEPS + COLUMNS_HALO_STEPS; i++)
    {
        s_Data[threadIdx.x][threadIdx.y + i * COLUMNS_BLOCKDIM_Y]= (imageH - baseY > i * COLUMNS_BLOCKDIM_Y) ? d_Src[i * COLUMNS_BLOCKDIM_Y * pitch] : 0;
    }

    //Compute and store results
    __syncthreads();
#pragma unroll

    for (int i = COLUMNS_HALO_STEPS; i < COLUMNS_HALO_STEPS + COLUMNS_RESULT_STEPS; i++)
    {
        float sum = 0;
#pragma unroll

        for (int j = -kernelRadius; j <= kernelRadius; j++)
        {
            sum += c_Kernel[kernelRadius - j] * s_Data[threadIdx.x][threadIdx.y + i * COLUMNS_BLOCKDIM_Y + j];
        }

        d_Dst[i * COLUMNS_BLOCKDIM_Y * pitch] = sum;
    }
}


//===============================================================
template<class imgType>
int separableConvolution_CUDA<imgType>::separableConvolutionOutOfPlace()
{

	std::cout<<"ERROR: separableConvolution_CUDA<imgType>::separableConvolutionOutOfPlace: code not adapted for 3D yet from NVIDIA sample. Main issue is that we have to pad the image to have specific size and duplicate image memory in GPU"<<std::endl;
	return 0;
	const int imageW = imgDims[0];
	const int imageH = imgDims[1];
	const int imageD = imgDims[2];

	assert(ROWS_BLOCKDIM_X * ROWS_HALO_STEPS >= kernel_radius);
    assert(imageW % (ROWS_RESULT_STEPS * ROWS_BLOCKDIM_X) == 0);
    assert(imageH % ROWS_BLOCKDIM_Y == 0);
	assert(imageD % ROWS_BLOCKDIM_Z == 0);

	dim3 blocks(imageW / (ROWS_RESULT_STEPS * ROWS_BLOCKDIM_X), imageH / ROWS_BLOCKDIM_Y, imageD / ROWS_BLOCKDIM_Z);
	dim3 threads(ROWS_BLOCKDIM_X, ROWS_BLOCKDIM_Y, ROWS_BLOCKDIM_Z);


	//alocate second set of float
	float* img_CUDAbuffer2;
	HANDLE_ERROR(cudaMalloc((void**)&(img_CUDAbuffer2), numElements() * sizeof(float)));

	switch(kernel_radius)
	{
	case 1:
		convolutionRowsKernel<1><<<blocks, threads>>>(img_CUDAbuffer2, img_CUDAbuffer, imageW, imageH, imageD);HANDLE_ERROR_KERNEL;
		break;
	case 2:
		convolutionRowsKernel<2><<<blocks, threads>>>(img_CUDAbuffer2, img_CUDAbuffer, imageW, imageH, imageD);HANDLE_ERROR_KERNEL;
		break;
	case 3:
		convolutionRowsKernel<3><<<blocks, threads>>>(img_CUDAbuffer2, img_CUDAbuffer, imageW, imageH, imageD);HANDLE_ERROR_KERNEL;
		break;
	case 4:
		convolutionRowsKernel<4><<<blocks, threads>>>(img_CUDAbuffer2, img_CUDAbuffer, imageW, imageH, imageD);HANDLE_ERROR_KERNEL;
		break;
	case 5:
		convolutionRowsKernel<5><<<blocks, threads>>>(img_CUDAbuffer2, img_CUDAbuffer, imageW, imageH, imageD);HANDLE_ERROR_KERNEL;
		break;
	case 6:
		convolutionRowsKernel<6><<<blocks, threads>>>(img_CUDAbuffer2, img_CUDAbuffer, imageW, imageH, imageD);HANDLE_ERROR_KERNEL;
		break;
	case 7:
		convolutionRowsKernel<7><<<blocks, threads>>>(img_CUDAbuffer2, img_CUDAbuffer, imageW, imageH, imageD);HANDLE_ERROR_KERNEL;
		break;
	case 8:
		convolutionRowsKernel<8><<<blocks, threads>>>(img_CUDAbuffer2, img_CUDAbuffer, imageW, imageH, imageD);HANDLE_ERROR_KERNEL;
		break;
	case 9:
		convolutionRowsKernel<9><<<blocks, threads>>>(img_CUDAbuffer2, img_CUDAbuffer, imageW, imageH, imageD);HANDLE_ERROR_KERNEL;
		break;
	case 10:
		convolutionRowsKernel<10><<<blocks, threads>>>(img_CUDAbuffer2, img_CUDAbuffer, imageW, imageH, imageD);HANDLE_ERROR_KERNEL;
		break;
	case 11:
		convolutionRowsKernel<11><<<blocks, threads>>>(img_CUDAbuffer2, img_CUDAbuffer, imageW, imageH, imageD);HANDLE_ERROR_KERNEL;
		break;
	default:
		std::cout<<"ERROR: code not ready for this kernel radius "<<kernel_radius<<std::endl;
	}
	/*
	//convolution columns
	assert(COLUMNS_BLOCKDIM_Y * COLUMNS_HALO_STEPS >= kernel_radius);
    assert(imageW % COLUMNS_BLOCKDIM_X == 0);
    assert(imageH % (COLUMNS_RESULT_STEPS * COLUMNS_BLOCKDIM_Y) == 0);

    dim3 blocks(imageW / COLUMNS_BLOCKDIM_X, imageH / (COLUMNS_RESULT_STEPS * COLUMNS_BLOCKDIM_Y));
    dim3 threads(COLUMNS_BLOCKDIM_X, COLUMNS_BLOCKDIM_Y);

    convolutionColumnsKernel<<<blocks, threads>>>(d_Dst, d_Src, imageW, imageH, imageW);HANDLE_ERROR_KERNEL;
	*/
	
	


	if( (dimsImage % 2) == 1) //transfer results to origianl float buffer
	{
		float *auxSwap = img_CUDAbuffer;
		img_CUDAbuffer = img_CUDAbuffer2;
		img_CUDAbuffer2 = auxSwap;
	}

	//release temporary out of place memory
	HANDLE_ERROR( cudaFree(img_CUDAbuffer2) );
	
	return 0;
	
}

//===================================================================================
//in tests with CUDA architecture 2.0 tends to be 7X slower (due to non-coalescent access of memory). However, it requires half the memory and it can do convolution in N-D images
//
template<class imgType>
int separableConvolution_CUDA<imgType>::separableConvolutionInPlace()
{
	std::int64_t volSize = numElements();


	for(int ii = 0; ii < dimsImage; ii++)
	{
		int blocks = std::min((int)(volSize/imgDims[ii]), (int) MAX_BLOCKS_CUDA);
		int threads = std::min((int)(imgDims[ii]), (int)MAX_THREADS_CUDA_SCONV);
		
		
		if( imgDims[ii] <= 128 )
		{
			separableConvolutionInplaceKernel<128,dimsImage><<<blocks,threads>>>(img_CUDAbuffer,kernel_radius,ii, volSize);HANDLE_ERROR_KERNEL;
		}
		else if( imgDims[ii] <= 256 ){
			separableConvolutionInplaceKernel<256,dimsImage><<<blocks,threads>>>(img_CUDAbuffer,kernel_radius,ii, volSize);HANDLE_ERROR_KERNEL;}
		else if( imgDims[ii] <= 512 ){
			separableConvolutionInplaceKernel<512,dimsImage><<<blocks,threads>>>(img_CUDAbuffer,kernel_radius,ii, volSize);HANDLE_ERROR_KERNEL;}
		else if( imgDims[ii] <= 1024 ){
			separableConvolutionInplaceKernel<1024,dimsImage><<<blocks,threads>>>(img_CUDAbuffer,kernel_radius,ii, volSize);HANDLE_ERROR_KERNEL;}
		else if( imgDims[ii] <= 2048 ){
			separableConvolutionInplaceKernel<2048,dimsImage><<<blocks,threads>>>(img_CUDAbuffer,kernel_radius,ii, volSize);HANDLE_ERROR_KERNEL;}
		else if( imgDims[ii] <= 4096 ){
			separableConvolutionInplaceKernel<4096,dimsImage><<<blocks,threads>>>(img_CUDAbuffer,kernel_radius,ii, volSize);HANDLE_ERROR_KERNEL;}
		else
			printf("ERROR: image is too large for this code. We run out of share memory\n");
		
	}

	return 0;
}

//===================================================================================
//in tests with CUDA architecture 2.0 tends to be 7X slower (due to non-coalescent access of memory). However, it requires half the memory and it can do convolution in N-D images
//in tests it is 10X faster than Matlab
template<class imgType>
int separableConvolution_CUDA<imgType>::separableConvolutionInPlace(float* externImgCUDA, std::int64_t imgDims_[dimsImage], int kernelRadius_)
{
	//you need to load image into CUDA
	//you need to call updateKernel and updateImageDimensionConstant to set constant memory in GPU
	
	std::int64_t volSize = 1;
	for(int ii = 0; ii < dimsImage; ii++)
		volSize *= imgDims_[ii];

	for(int ii = 0; ii < dimsImage; ii++)
	{
		int blocks = std::min((int)(volSize/imgDims_[ii]), (int) MAX_BLOCKS_CUDA);
		int threads = std::min((int)(imgDims_[ii]), (int)MAX_THREADS_CUDA_SCONV);
		
		
		if( imgDims_[ii] <= 128 )
		{
			separableConvolutionInplaceKernel<128,dimsImage><<<blocks,threads>>>(externImgCUDA,kernelRadius_,ii, volSize);HANDLE_ERROR_KERNEL;
		}
		else if( imgDims_[ii] <= 256 ){
			separableConvolutionInplaceKernel<256,dimsImage><<<blocks,threads>>>(externImgCUDA,kernelRadius_,ii, volSize);HANDLE_ERROR_KERNEL;}
		else if( imgDims_[ii] <= 512 ){
			separableConvolutionInplaceKernel<512,dimsImage><<<blocks,threads>>>(externImgCUDA,kernelRadius_,ii, volSize);HANDLE_ERROR_KERNEL;}
		else if( imgDims_[ii] <= 1024 ){
			separableConvolutionInplaceKernel<1024,dimsImage><<<blocks,threads>>>(externImgCUDA,kernelRadius_,ii, volSize);HANDLE_ERROR_KERNEL;}
		else if( imgDims_[ii] <= 2048 ){
			separableConvolutionInplaceKernel<2048,dimsImage><<<blocks,threads>>>(externImgCUDA,kernelRadius_,ii, volSize);HANDLE_ERROR_KERNEL;}
		else if( imgDims_[ii] <= 4096 ){
			separableConvolutionInplaceKernel<4096,dimsImage><<<blocks,threads>>>(externImgCUDA,kernelRadius_,ii, volSize);HANDLE_ERROR_KERNEL;}
		else
			printf("ERROR: image is too large for this code. We run out of share memory\n");
		
	}

	return 0;
}

//===================================================================================
//in tests with CUDA architecture 2.0 tends to be 7X slower (due to non-coalescent access of memory). However, it requires half the memory and it can do convolution in N-D images
//in tests it is 10X faster than Matlab
template<class imgType>
int separableConvolution_CUDA<imgType>::separableConvolutionInPlace(float* externImgCUDA, std::int64_t imgDims_[dimsImage], int kernelRadius_, int dim)
{
	//you need to load image into CUDA
	//you need to call updateKernel and updateImageDimensionConstant to set constant memory in GPU
	
	std::int64_t volSize = 1;
	for(int jj = 0; jj < dimsImage; jj++)
		volSize *= imgDims_[jj];

	
	int blocks = std::min((int)(volSize/imgDims_[dim]), (int) MAX_BLOCKS_CUDA);
	int threads = std::min((int)(imgDims_[dim]), (int)MAX_THREADS_CUDA_SCONV);

	if( imgDims_[dim] <= 128 )
	{
		separableConvolutionInplaceKernel<128,dimsImage><<<blocks,threads>>>(externImgCUDA,kernelRadius_,dim, volSize);HANDLE_ERROR_KERNEL;
	}
	else if( imgDims_[dim] <= 256 ){
		separableConvolutionInplaceKernel<256,dimsImage><<<blocks,threads>>>(externImgCUDA,kernelRadius_,dim, volSize);HANDLE_ERROR_KERNEL;}
	else if( imgDims_[dim] <= 512 ){
		separableConvolutionInplaceKernel<512,dimsImage><<<blocks,threads>>>(externImgCUDA,kernelRadius_,dim, volSize);HANDLE_ERROR_KERNEL;}
	else if( imgDims_[dim] <= 1024 ){
		separableConvolutionInplaceKernel<1024,dimsImage><<<blocks,threads>>>(externImgCUDA,kernelRadius_,dim, volSize);HANDLE_ERROR_KERNEL;}
	else if( imgDims_[dim] <= 2048 ){
		separableConvolutionInplaceKernel<2048,dimsImage><<<blocks,threads>>>(externImgCUDA,kernelRadius_,dim, volSize);HANDLE_ERROR_KERNEL;}
	else if( imgDims_[dim] <= 4096 ){
		separableConvolutionInplaceKernel<4096,dimsImage><<<blocks,threads>>>(externImgCUDA,kernelRadius_,dim, volSize);HANDLE_ERROR_KERNEL;}
	else
		printf("ERROR: image is too large for this code. We run out of share memory\n");



	return 0;
}

//=====================================================
//declare all the possible types so template compiles properly
template separableConvolution_CUDA<unsigned short int>;
template separableConvolution_CUDA<unsigned char>;
template separableConvolution_CUDA<float>;