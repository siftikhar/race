/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifndef METADATAFILTER_H
#define METADATAFILTER_H

// qt header
#include <QtCore/QList>
#include <QtCore/QStringList>


namespace XPIWIT
{

/**
 * @class MetaDataFilter
 * @brief The MetaDataFilter class provides meta information and data output from a filter
 */
class MetaDataFilter
{
	public:
		MetaDataFilter();
		~MetaDataFilter();

		QStringList mTitle;
		QStringList mType;
		QList< QList< float > > mData;

		QString mFilterName;
		QString mFilterId;
		QString mPostfix;

		QString mOutputPath;

		bool mIsMultiDimensional;
		bool mWriteMetaData;

		void WriteFilter( QString path, bool useHeader, QString separator, QString delimiter );
		const int GetFeatureIndex(const QString& featureName);
	private:
};

} //namespace XPIWIT

#endif // METADATAFILTER_H

