/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// project header
#include "MedianImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkMedianImageFilter.h"


namespace XPIWIT
{

// the default constructor
template< class TInputImage >
MedianImageFilterWrapper<TInputImage>::MedianImageFilterWrapper() : ProcessObjectBase()
{
    // set the widget type
    this->mName = MedianImageFilterWrapper<TInputImage>::GetName();
    this->mDescription = "Median Filter. ";
    this->mDescription += "Filters the input with a median kernel.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings to the filter
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "Radius", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Radius of the filter kernel (manhattan distance)." );
    processObjectSettings->AddSetting( "FilterMask3D", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Use a 3D kernel." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
MedianImageFilterWrapper<TInputImage>::~MedianImageFilterWrapper()
{
}


// update the filter
template< class TInputImage >
void MedianImageFilterWrapper<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const int size = processObjectSettings->GetSettingValue( "Radius" ).toInt();
    const int filterMask3D = processObjectSettings->GetSettingValue( "FilterMask3D" ).toInt() > 0;
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;

	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();

    // set the parameters
    typedef itk::MedianImageFilter<TInputImage, TInputImage> MedianFilterType;
    typename MedianFilterType::Pointer filter = MedianFilterType::New();

    filter->SetReleaseDataFlag( releaseDataFlag );
    filter->SetInput( inputImage );
    filter->SetNumberOfThreads( maxThreads );

    typename MedianFilterType::InputSizeType radius;
    radius.Fill(size);
    if (filterMask3D == false && TInputImage::ImageDimension > 2)
        radius.SetElement(2, 0);
    filter->SetRadius(radius);

    // update the filter
    itkTryCatch( filter->Update(), "Error: MedianFilterWidget Update Function." );
    ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( filter->GetOutput() );
    mOutputImages.append( outputImage );

    // update the process update widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< MedianImageFilterWrapper<Image2Float> > MedianImageFilterWrapperImage2Float;
static ProcessObjectProxy< MedianImageFilterWrapper<Image3Float> > MedianImageFilterWrapperImage3Float;
static ProcessObjectProxy< MedianImageFilterWrapper<Image2UShort> > MedianImageFilterWrapperImage2UShort;
static ProcessObjectProxy< MedianImageFilterWrapper<Image3UShort> > MedianImageFilterWrapperImage3UShort;

} // namespace XPIWIT
