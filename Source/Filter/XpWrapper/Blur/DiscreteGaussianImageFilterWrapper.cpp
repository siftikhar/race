/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// project header
#include "DiscreteGaussianImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkDiscreteGaussianImageFilter.h"


namespace XPIWIT
{

// the default constructor
template< class TInputImage >
DiscreteGaussianImageFilterWrapper<TInputImage>::DiscreteGaussianImageFilterWrapper() : ProcessObjectBase()
{
    // set the filter name
    this->mName = DiscreteGaussianImageFilterWrapper<TInputImage>::GetName();
    this->mDescription = "Gaussian smothing filter. ";
    this->mDescription += "Filters the image with a gaussian kernel defined by variance.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
    this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "Variance", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Variance of the gaussian kernel." );
    processObjectSettings->AddSetting( "MaximumError", "0.01", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Maximum error of the gaussian function approximation." );
    processObjectSettings->AddSetting( "MaximumKernelWidth", "32", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Maximum kernel size in pixel." );
    processObjectSettings->AddSetting( "UseImageSpacing", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Use the real spacing for the gaussian kernel creation."  );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
DiscreteGaussianImageFilterWrapper<TInputImage>::~DiscreteGaussianImageFilterWrapper()
{
}


// the update function
template< class TInputImage >
void DiscreteGaussianImageFilterWrapper<TInputImage>::Update()
{
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const float variance = processObjectSettings->GetSettingValue( "Variance" ).toDouble();

    // get images
    typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
    ProcessObjectBase::StartTimer();

    // setup the gaussian filter
    typedef itk::DiscreteGaussianImageFilter<TInputImage, TInputImage> GaussianFilter;
    typename GaussianFilter::Pointer filter = GaussianFilter::New();
    filter->SetInput( inputImage );
    filter->SetReleaseDataFlag( true );
    filter->SetVariance( variance );
    filter->SetMaximumError( processObjectSettings->GetSettingValue( "MaximumError" ).toDouble() );
    filter->SetMaximumKernelWidth( processObjectSettings->GetSettingValue( "MaximumKernelWidth" ).toInt() );
    filter->SetUseImageSpacing( processObjectSettings->GetSettingValue( "UseImageSpacing" ).toInt() );
    filter->SetNumberOfThreads( maxThreads );

    itkTryCatch(filter->Update(), "Error: DiscreteGaussianImageFilterWrapper Update Function.");

    ImageWrapper *outputWrapper = new ImageWrapper();
    outputWrapper->SetImage<TInputImage>( filter->GetOutput() );
    mOutputImages.append( outputWrapper );

    // log performance and write results
    ProcessObjectBase::LogPerformance();
    ProcessObjectBase::Update();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< DiscreteGaussianImageFilterWrapper<Image2Float> > DiscreteGaussianImageFilterWrapperImage2Float;
static ProcessObjectProxy< DiscreteGaussianImageFilterWrapper<Image3Float> > DiscreteGaussianImageFilterWrapperImage3Float;
static ProcessObjectProxy< DiscreteGaussianImageFilterWrapper<Image2UShort> > DiscreteGaussianImageFilterWrapperImage2UShort;
static ProcessObjectProxy< DiscreteGaussianImageFilterWrapper<Image3UShort> > DiscreteGaussianImageFilterWrapperImage3UShort;

} // namespace XPIWIT

