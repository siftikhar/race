/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// namespace header
#include "LabelGeometryImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkLabelMapToLabelImageFilter.h"
#include "itkLabelGeometryImageFilter.h"

using namespace itk;


namespace XPIWIT
{

// the default constructor
template < class TInputImage >
LabelGeometryImageFilterWrapper< TInputImage >::LabelGeometryImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = LabelGeometryImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "Extract geometry information of labeled image";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(0);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(1);
	this->mObjectType->AppendMetaOutputType("GeometryProperties");

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TInputImage >
LabelGeometryImageFilterWrapper< TInputImage >::~LabelGeometryImageFilterWrapper()
{
}


// the update function
template < class TInputImage >
void LabelGeometryImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	
	// get images
	typedef itk::Image< unsigned short, TInputImage::ImageDimension >  LabelType;
	typename LabelType::Pointer inputImage = mInputImages.at(0)->template GetImage<LabelType>();
	
	// start processing
	ProcessObjectBase::StartTimer();
	
	// setup the filter
	typedef itk::LabelGeometryImageFilter<LabelType, LabelType> LabelGeometryImageFilterType;
    typename LabelGeometryImageFilterType::Pointer labelGeometryFilter =  LabelGeometryImageFilterType::New();
	labelGeometryFilter->SetInput( inputImage );
    labelGeometryFilter->SetCalculateOrientedBoundingBox( true );
    labelGeometryFilter->SetCalculateOrientedIntensityRegions( false );
    labelGeometryFilter->SetCalculateOrientedLabelRegions( false );
    labelGeometryFilter->SetCalculatePixelIndices( false );
    labelGeometryFilter->SetReleaseDataFlag( true );
	
	itkTryCatch(labelGeometryFilter->Update(), "Error: LabelGeometryImageFilterWrapper Update Function.");

	// Create the meta data Object
	MetaDataFilter* metaOutput = this->mMetaOutputs.at(0);
    metaOutput->mIsMultiDimensional = true;
	metaOutput->mPostfix = "RegionProps";

	QStringList metaDescription;                    QStringList metaType;
    metaDescription << "id";						metaType << "int";
	metaDescription << "size";						metaType << "int";
	metaDescription << "xpos";						metaType << "int";	// centroid
	metaDescription << "ypos";						metaType << "int";
	if( TInputImage::ImageDimension >= 3 )
		metaDescription << "zpos";					metaType << "int";

	metaDescription << "bbOriginX";					metaType << "int";	// bounding box origin
	metaDescription << "bbOriginY";					metaType << "int";
	if( TInputImage::ImageDimension >= 3 )
		metaDescription << "bbOriginZ";				metaType << "int";
	metaDescription << "bbSizeX";					metaType << "int";	// bounding box size
	metaDescription << "bbSizeY";					metaType << "int";
	if( TInputImage::ImageDimension >= 3 )
		metaDescription << "bbSizeZ";				metaType << "int";

	metaDescription << "xweighted";					metaType << "int";	// weighted centroid
	metaDescription << "yweighted";					metaType << "int";
	if( TInputImage::ImageDimension >= 3 )
		metaDescription << "zweighted";				metaType << "int";
	metaDescription << "integrated intensity";		metaType << "int";
	//metaDescription << "axes length";				metaType << "int";
	metaDescription << "major axis length";			metaType << "int";
	metaDescription << "minor axis length";			metaType << "int";
	metaDescription << "eccentricity";				metaType << "float";
	metaDescription << "elongation";				metaType << "float";
	metaDescription << "orientation";				metaType << "float";

	// write to meta object
	metaOutput->mTitle = metaDescription;
	metaOutput->mType = metaType;

	// get the labels
    typename LabelGeometryImageFilterType::LabelsType allLabels = labelGeometryFilter->GetLabels();
    typename LabelGeometryImageFilterType::LabelsType::iterator allLabelsIt;
    unsigned int currentKeyPointID = 0;
    // iterate trough the labels and extract nucleus information
    for( allLabelsIt = allLabels.begin(); allLabelsIt != allLabels.end(); allLabelsIt++ )
    {
        ++currentKeyPointID;
        typename LabelGeometryImageFilterType::LabelPixelType labelValue = *allLabelsIt;

		QList<float> currentData;
		currentData << currentKeyPointID;											// id
		currentData << labelGeometryFilter->GetVolume(labelValue);					// size
		currentData << labelGeometryFilter->GetCentroid(labelValue)[0];				// xpos			// centroid
		currentData << labelGeometryFilter->GetCentroid(labelValue)[1];				// ypos
		if( TInputImage::ImageDimension >= 3 )
			currentData << labelGeometryFilter->GetCentroid(labelValue)[2];			// zpos
		
		// bounding box origin
		currentData << labelGeometryFilter->GetBoundingBox(labelValue)[0];			
		currentData << labelGeometryFilter->GetBoundingBox(labelValue)[2];
		if( TInputImage::ImageDimension >= 3 )
			currentData << labelGeometryFilter->GetBoundingBox(labelValue)[4];

		// bounding box size
		currentData << labelGeometryFilter->GetBoundingBoxSize(labelValue)[0];		
		currentData << labelGeometryFilter->GetBoundingBoxSize(labelValue)[1];
		if( TInputImage::ImageDimension >= 3 )
			currentData << labelGeometryFilter->GetBoundingBoxSize(labelValue)[2];

		currentData << labelGeometryFilter->GetWeightedCentroid(labelValue)[0];		// xweighted	// weighted centroid
		currentData << labelGeometryFilter->GetWeightedCentroid(labelValue)[1];		// yweighted
		if( TInputImage::ImageDimension >= 3 )
			currentData << labelGeometryFilter->GetWeightedCentroid(labelValue)[2];	// zweighted
		currentData << labelGeometryFilter->GetIntegratedIntensity(labelValue);		// integrated intensity
		// avarege: labelGeometryFilter->GetIntegratedIntensity(labelValue) / labelGeometryFilter->GetVolume(labelValue)
		//currentData << labelGeometryFilter->GetAxesLength(labelValue) );				// axes length
		currentData << labelGeometryFilter->GetMajorAxisLength(labelValue);			// major axis length
		currentData << labelGeometryFilter->GetMinorAxisLength(labelValue);			// minor axis length
		currentData << labelGeometryFilter->GetEccentricity(labelValue);				// eccentricity
		currentData << labelGeometryFilter->GetElongation(labelValue);				// elongation
		currentData << labelGeometryFilter->GetOrientation(labelValue);				// orientation

		metaOutput->mData.append( currentData );
    }

	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< LabelGeometryImageFilterWrapper<Image2Float> > LabelGeometryImageFilterWrapperImage2Float;
static ProcessObjectProxy< LabelGeometryImageFilterWrapper<Image3Float> > LabelGeometryImageFilterWrapperImage3Float;
static ProcessObjectProxy< LabelGeometryImageFilterWrapper<Image2UShort> > LabelGeometryImageFilterWrapperImage2UShort;
static ProcessObjectProxy< LabelGeometryImageFilterWrapper<Image3UShort> > LabelGeometryImageFilterWrapperImage3UShort;

} // namespace XPIWIT

